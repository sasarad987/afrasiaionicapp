/**
 * Login type: Guest,Email,Fb,Google
 * Default Login type:Guest
 */

AfrisiaApp
        .controller('MainCtrl', function ($scope, $rootScope, $cordovaNetwork, $cordovaCamera, $cordovaFile, $ionicActionSheet, $cordovaInAppBrowser, $cordovaFacebook, $localStorage, $location, $cordovaGooglePlus, $ionicHistory, $ionicSideMenuDelegate, $state, $ionicPopup, $ionicLoading, APIService, User, Cart) {
            $scope.init = function () {
                //User manage part
                $rootScope.loginType = User.getLoginType();
                $rootScope.loggedin = User.isLoggedIn();
                $scope.user = User.profile();
                $rootScope.currentAddress = '';
                $rootScope.login_data = {
                    user_email: '',
                    user_password: '',
                };
                $rootScope.register_data = {
                    signup_mode: "",
                    user_name:"",
                    user_password:"",
                    user_email:"",
                    user_photo_data:"",
                    user_search_text1:"",
                    user_search_text2:"",
                    user_favor_color:"",
                };
                $rootScope.forgot_data = {
                    user_email:''
                }
                $rootScope.capture_image = '';
                $rootScope.bussinessType= [
                    {
                        "type_id": null,
                        "business_type": null
                    }
                
                ];
                                
                /////////////////////////////////////////////
                $rootScope.active_label = null;
                console.log("dsffdsfdsfdsfdsfdsfdsfds");
                // $rootScope.loadBusinessType = function () {
                //     var temp = {
                        
                //         type_base: "business"
                //     }
                       
                //     APIService.getBusinessType(
                        
                //         temp,
                //         function(result){
                //             if(result.status){
                //                 if(result.result){
                //                     $rootScope.bussinessType=JSON.stringify(result.result);
                                    
                                    
                                   
                //                     //Refresh
                            
                //                     // $state.go($state.current, {}, {reload: true});
                //                 }
                //                 else{
                //                     $ionicPopup.alert({
                //                         title: 'Info',
                //                         template: result.msg//JSON.stringify(result.description.error)
                                    
                //                     });
                //                     $scope.active_search="";
                                
                //                 }
                            
                //             }else{
                //                 $ionicPopup.alert({
                //                     title: 'Info',
                //                     template: "Fill in the blank text correctly"//JSON.stringify(result.description.error)
                //                 });
                //             }

                           
                //         },
                //         function (err) {
                //             $ionicLoading.hide();
                //             $ionicPopup.alert({
                //                 title: 'Error',
                //                 template: 'Server connect error'//JSON.stringify(err)
                //             });

                          
                //         });
                // };
                            
                $scope.$on('$ionicView.afterEnter', function() {
                    $rootScope.loginType = User.getLoginType();
                    $rootScope.loggedin = User.isLoggedIn();
                    // $rootScope.loadBusinessType();
                    console.log("dsffdsfdsfdsfdsfdsffdssssssssssssssssssssdsfds");

                    if($rootScope.loggedin){
                        // $location.path("/resort-list");
                        var user = User.profile();
                        $scope.login_data = {
                            user_email: user.user_email,
                            user_password: user.user_password,
                        };
                        console.log("mainctrl:"+user);
                        User.login(
                            $scope.login_data,
                            'email',
                            function () {
                                $rootScope.updateUser();
                            },
                            function (error) {
                                User.logout();
                                $rootScope.updateUser();
                                $rootScope.$broadcast('user:logout',User.profile());
                                //Cart related                        
                                $location.path('/resort-list');
                            }
                        );
                        $rootScope.$broadcast('userLoggedIn');
                     
       
                
      
                
       
                    }               
                });
                $scope.$on('$ionicView.enter', function(){
                    $ionicSideMenuDelegate.canDragContent(false);
                });
                $scope.$on('$ionicView.leave', function(){
                    $ionicSideMenuDelegate.canDragContent(true);
                });
                $scope.$on('user:logout', function(data) {
                    $scope.user = User.profile();
                    $rootScope.loggedin = User.isLoggedIn();
                    $rootScope.updateUser();
                });

                $ionicHistory.nextViewOptions({
                    //disableAnimate: true,
                    disableBack: true
                });
            }
        })
       .controller('NavCtrl', function($scope, $state, $rootScope, $location, $ionicHistory,$ionicLoading, $localStorage, $ionicSideMenuDelegate, User) {

            $scope.init = function(){
                $scope.user_image = null;
                $scope.user_name = null;
                $rootScope.updateUser();
                $scope.label = {
                    user_id: "",
                    business_name: "",
                    business_type: "",
                    business_address:"",
                    search_text: ""
                    
                }
                var user = User.profile();
                if( User.isLoggedIn()){
                    
                    $scope.user_image = user.user_photo_url;
                    $scope.user_name = user.user_name;
                    $scope.business_count=user.user_business_count;
                    $scope.comment_count=user.user_comment_count;
                    $scope.favorit_count=user.user_favor_count;
                    
                }
            
            }

            $rootScope.showMenu = function () {
                $rootScope.flag=1;
                $ionicSideMenuDelegate.toggleLeft();
            };
            

            $rootScope.showRightMenu = function () {
                $location.path('/about');
                //$ionicSideMenuDelegate.toggleRight();
            } ;
            // go to profile
            $rootScope.goToProfile = function () {
                $state.go('profile');
            };

            $rootScope.$on('userLoggedIn',function() {
                var user = User.profile();
                $scope.user_image = user.user_photo_url;
                $scope.user_name = user.user_name;
                $scope.loginType = User.getLoginType();
                $scope.loggedin = User.isLoggedIn();
                $scope.business_count=user.user_business_count;
                $scope.comment_count=user.user_comment_count;
                $scope.favorit_count=user.user_favor_count;
                console.log("dsfdsfsadfdsfdsfds::::::::"+user);
                //Label
                $scope.label.user_id=user.user_id;
                $scope.label.search_text=user.user_search_text1+","+user.user_search_text2;
                
                console.log($scope.label);
                $localStorage.label=JSON.stringify($scope.label);
                console.log($localStorage.label);
            
                
                
            
            });
             $rootScope.$on('reLogin',function() {
                var user = User.profile();
                $rootScope.login_data = {
                    
                     user_email: user.user_email,
                     user_password: user.user_password
                };
                console.log($rootScope.login_data);
                console.log(user);
                
                User.login(
                            $rootScope.login_data,
                            'email',
                            function () {
                                $ionicLoading.hide();
                                $rootScope.updateUser();
                            },
                            function (error) {
                                User.logout();
                                console.log("detail3");
                                $rootScope.updateUser();
                                $rootScope.$broadcast('user:logout',User.profile());
                                //Cart related                        
                                $location.path('/resort-list');
                            }
                        );
                        $rootScope.$broadcast('userLoggedIn');
            
                
                
            
            });


            $rootScope.updateUser = function () {
                var user = User.profile();
                $scope.user_image = user.user_photo_url;
                $scope.user_name = user.user_name;
                $scope.loginType = User.getLoginType();
                $scope.loggedin = User.isLoggedIn();
                $scope.business_count=user.user_business_count;
                $scope.comment_count=user.user_comment_count;
                $scope.favorit_count=user.user_favor_count;
                
                
            };

            $scope.login = function(){
                $location.path('/login');
            };
    
            $scope.contact = function(){
                $location.path('/contact');
            };
            $scope.goTomyInstall = function(){
                $location.path('/myinstall');
            };
            $scope.goTomyfavorit = function(){
                $location.path('/myfavorit');
            };
            $scope.goTomyComment = function(){
                $location.path('/mycomment');
            };
        

            $scope.signup = function(){
                $location.path('/register');
            };
    
    
            $scope.logout = function(){
               
                //User related
                User.logout();
                $rootScope.updateUser();
                $rootScope.$broadcast('user:logout',User.profile());
          
       

                $ionicLoading.show('Logout..');
                setTimeout(function () {
                    $ionicLoading.hide();
                
                    // $rootScope.researchList();
                    $state.go('resort-list');
                },1500);
                // $location.path('/resort-list');
                $ionicSideMenuDelegate.toggleLeft(false);
                $rootScope.$broadcast('user:logout',User.profile());
            
        
            
            };

            $scope.$on('$ionicView.afterEnter', function() {
                $ionicLoading.hide();
                $rootScope.updateUser();
                console.log($localStorage.label);
            
            
            });
            $scope.$on('user:logout', function() {
                $rootScope.updateUser();
            
            });

        
     })
    

     
    .controller('ResortListCtrl', function ($scope, $cordovaGeolocation, ModalService, $ionicScrollDelegate, $rootScope, $state, $cordovaSocialSharing, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService, User) {
        //  console.log(item);
       $scope.closeMyModal = function(){
           $rootScope.researchList1();
           $scope.closeModal();
       }
      

        $scope.init = function () {
            $scope.label = {
                user_id: "",
                business_name: "",
                business_type: "",
                business_address:"",
                search_text: ""
                
            }
            $scope.ll = {
                active_search_s: ""
            }
            $scope.labelList = [
                {
                    label_name: "",
                    label_rate: ""
                }
            
           ];
      
            if(!$localStorage.label){
                $localStorage.label=JSON.stringify($scope.label);
            }
            $scope.data = {
                showDelete: false
            };
            $scope.items=[];
            $scope.active_search="";
            $rootScope.active_business = null;
            console.log("fadsffd::::::"+$scope.business_type);
            
            $scope.loadResortList();
            $scope.loadBusinessType();
            
            
            
          
        }
        // if($localStorage.label){
        //     $scope.label=JSON.parse($localStorage.label);
        // }
        $scope.searchLabel = function(item){
            console.log(item.label_name);
            $scope.ll.active_search_s = item.label_name;
            console.log($scope.ll.active_search_s);
            $scope.active_search = item.label_name;
            console.log( $scope.active_search);
        }
        $scope.goLabelChoose = function(){
         
        
            ModalService
            .init('templates/labelSearch.html', $scope)
            .then(function (modal) {
                $scope.ll.active_search_s = "";
                $scope.active_search = "";
                modal.show();
            });
         
        }
        
        

            $scope.loadBusinessType = function () {
                    var temp = {
                        
                        type_base: "business"
                    }
                       
                    APIService.getBusinessType(
                        
                        temp,
                        function(result){
                            if(result.status){
                                if(result.result){
                                    $scope.bussinessType=JSON.stringify(result.result);
                                    $localStorage.bussinessType=JSON.stringify(result.result);
                                    
                                    
                                    
                                    console.log("Business_type"+ $scope.bussinessType);
                                    console.log( $rootScope.bussinessType);
                                   
                                    //Refresh
                            
                                    // $state.go($state.current, {}, {reload: true});
                                }
                                else{
                                    // $ionicPopup.alert({
                                    //     title: 'Info',
                                    //     template: result.msg//JSON.stringify(result.description.error)
                                    
                                    // });
                                    $scope.active_search="";
                                
                                }
                            
                            }else{
                                $ionicPopup.alert({
                                    title: 'Info',
                                    template: "Fill in the blank text correctly"//JSON.stringify(result.description.error)
                                });
                            }

                           
                        },
                        function (err) {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Error',
                                template: 'Server connect error'//JSON.stringify(err)
                            });

                          
                        });
            };

            // $scope.share = function(item) {
            // console.log(item);

            // var resort = item;
            // console.log("name1"+resort.business_photo_url);
            // try{
            //     // $ionicLoading.show({template:'Find SNS..'});
            //     $cordovaSocialSharing
            //     .shareViaFacebook("This is a Test!",  resort.business_photo_url, null)
            //         .then(function(result) {
            //             // $ionicLoading.hide();
            //         }, function(error) {
            //             $ionicLoading.hide();
            //             $ionicPopup.alert({
            //                 title:'Error',
            //                 template:'Cannot share'
            //             });
            //         });
            // }catch (err){
            //     $ionicLoading.hide();
            //     $ionicPopup.alert({
            //         title: 'Alert',
            //         template: err.message
            //     });
            // }
        //      try{
        //         // $ionicLoading.show({template:'Find SNS..'});
        //         $cordovaSocialSharing
        //         .shareViaTwitter("This is a Test!",  resort.business_photo_url, null)
        //             .then(function(result) {
        //                 // $ionicLoading.hide();
        //             }, function(error) {
        //                 $ionicLoading.hide();
        //                 $ionicPopup.alert({
        //                     title:'Error',
        //                     template:'Cannot share'
        //                 });
        //             });
        //     }catch (err){
        //         $ionicLoading.hide();
        //         $ionicPopup.alert({
        //             title: 'Alert',
        //             template: err.message
        //         });
        //     }
        // };
        
        //         try{
        //         // $ionicLoading.show({template:'Find SNS..'});
        //         $cordovaSocialSharing
        //         .shareVia('linkedin',"This is a Test!", resort.business_photo_url, resort.business_photo_url, resort.business_photo_url)
        //             .then(function(result) {
        //                 // $ionicLoading.hide();
        //             }, function(error) {
        //                 $ionicLoading.hide();
        //                 $ionicPopup.alert({
        //                     title:'Error',
        //                     template:'Cannot share'
        //                 });
        //             });
        //     }catch (err){
        //         $ionicLoading.hide();
        //         $ionicPopup.alert({
        //             title: 'Alert',
        //             template: err.message
        //         });
        //     }
        // };
        //   window.plugins.socialsharing.shareVia('linkedin', 
		// 						title+": "+content,
		// 						null, 
		// 						card.img,  
		// 						card.lien, 
		// 						LoaderService.hide(), 
		// 						LoaderService.hide());
        
        $scope.moveItem = function(item, fromIndex, toIndex) {
            $scope.items.splice(fromIndex, 1);
            $scope.items.splice(toIndex, 0, item);
        };

        $scope.onItemDelete = function(item) {
            $scope.items.splice($scope.items.indexOf(item), 1);
        };
        $scope.openBusiness = function(item){





       
                $rootScope.active_business = item;
                //save active resort details
                $localStorage.active_business = JSON.stringify(item);
                $state.go('comment-view');
              
       
     
        }
        $scope.limitDistance = function(item){
            currentDistance = getDistanceFromLatLonInKm(CurrentLat, CurrentLng,item.business_lati, item.business_long);
            console.log(currentDistance);
            
            if(currentDistance <50){
                return true;
            }else{
                return false;
            }
            
        }
        $scope.changeFavor = function(item){
            $rootScope.flag=1;
            $rootScope.favor_flag=1;
            var user = User.profile();
            if(item.favor_status==1){
                
                var temp = { 
                    business_id: item.business_id,
                    user_id: user.user_id,
                    favor: "0"
                }
                console.log(user);
                console.log("iiiiiif"+temp);
                APIService.changeFavorit(
                        
                    temp,
                    function(result){
                        if(result.status){
                            
                                $ionicPopup.alert({
                                    title: 'Info',
                                    template: "Changed Successfully"//JSON.stringify(result.description.error)
                                });
                                item.favor_status=0;
                                $rootScope.$broadcast('reLogin');
                            }
                            else{
                                $ionicPopup.alert({
                                    title: 'Info',
                                    template: "You must user login."//JSON.stringify(result.description.error)
                                });                        
                            }                          
                                                    
                    },
                    function (err) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Error',
                            template: 'Server connect error'//JSON.stringify(err)
                        });

                        
                    });
            }else{
                
                
                temp = { 
                    business_id: item.business_id,
                    user_id: user.user_id,
                    favor:"1"
                }
                console.log(user);
                console.log("sdfdsf"+temp);
                APIService.changeFavorit(
                        
                    temp,
                    function(result){
                        if(result.status){
                            
                                $ionicPopup.alert({
                                    title: 'Info',
                                    template: "Changed Successfully"//JSON.stringify(result.description.error)
                                });
                                item.favor_status=1;
                                $rootScope.$broadcast('reLogin');
                            }
                            else{
                                $ionicPopup.alert({
                                    title: 'Info',
                                    template: "You must login."//JSON.stringify(result.description.error)
                                });                        
                            }                          
                                                    
                    },
                    function (err) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Error',
                            template: 'Server connect error'//JSON.stringify(err)
                        });

                        
                    });
            }
            
            
            
        }
        $scope.researchList = function (){
            // $scope.doRefresh();
          
            var newSearchText = $scope.active_search;
            newSearchText = newSearchText.replace(" ", "");
            $scope.label.search_text = newSearchText.replace(",", "@");
            console.log("aafdsffdsfd");
             $rootScope.researchList1();
            console.log( $scope.label.search_text);
            
            console.log($scope.label);
            $localStorage.label=JSON.stringify($scope.label);
            $scope.scrollMainToTop();
            // $scope.$broadcast('scroll.scrollTop');
           
            
        }
        $rootScope.researchList1 = function (){
            // $scope.doRefresh();
          
            $scope.label.search_text=$scope.active_search;
            $scope.loadResortList();
            console.log( $scope.label.search_text);
            console.log( $scope.active_search);
            console.log($scope.label);
            $localStorage.label=JSON.stringify($scope.label);
            $scope.scrollMainToTop();
            // $scope.$broadcast('scroll.scrollTop');
           
            
        }

        // $rootScope.$broadcast('userLoggedIn');
        $scope.loadResortList = function (callback) {
            var user=User.profile();
            if(User.isLoggedIn()){
                $scope.label.user_id = user.user_id;
                console.log("1:::"+$scope.label.user_id);
            }else{
                $scope.label.user_id="";
                console.log("0:::"+$scope.label.user_id);
            }
           
            $ionicLoading.show({
                template: 'Loading...'
            });
            
            APIService.resortList(
                
                $scope.label,
                function(result){
                    $ionicLoading.hide();
                    if(result.status){
                        if(result.result){
                            $scope.items=result.result;
                     
                            $scope.labelList = result.label_list;
                            $localStorage.labelList = JSON.stringify($scope.labelList);
                            console.log($scope.labelList);
                            console.log(result);
                            console.log($localStorage.label);
                            //Refresh
                    
                            $state.go($state.current, {}, {reload: true});
                        }
                        else{
                            $ionicPopup.alert({
                            title: 'Info',
                            template: result.msg//JSON.stringify(result.description.error)
                            
                            });
                            // $scope.active_search="";
                           
                        }
                    
                    }else{
                        $ionicPopup.alert({
                            title: 'Info',
                            template: "Fill in the blank text correctly"//JSON.stringify(result.description.error)
                        });
                    }

                    callback && callback();
                },
                function (err) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Server connect error'//JSON.stringify(err)
                    });

                    callback && callback();
                });
        };


        $scope.goResearch = function () {
            console.log($localStorage.label);
            // alert("research page");
            $scope.label.search_text=$scope.active_search;
            console.log("aafdsffdsfd");
            console.log($scope.active_search);
            
            if($localStorage.label){
               
                $localStorage.label=JSON.stringify($scope.label);
            }
            console.log($localStorage.label);
           
            $state.go('research'); 
    
            
        };
        $scope.goLeaveComment = function () {
            $state.go('comment-view');
        };

        
        
     
        // $scope.doRefresh = function () {
        //     // $scope.$route.reload();
        //     $scope.loadResortList(function () {
        //         $scope.$broadcast('scroll.refreshComplete');
        //     });
     
        // };
        $scope.scrollMainToTop = function() {
            $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
        };
        $scope.$on('$ionicView.beforeEnter', function() {
             console.log("$rootScope.flag:"+$rootScope.flag);
            if($rootScope.flag==1){
                $scope.init();
                console.log("detail2");
                $rootScope.flag=0;
                console.log("$rootScope.flag:"+$rootScope.flag);
            } 
        });


        $scope.$on('$ionicView.afterEnter', function() {
         
            // console.log("$rootScope.flag:"+$rootScope.flag);
            // if($rootScope.flag==1){
            //     $scope.init();
            //     console.log("detail2");
            //     $rootScope.flag=0;
            // }             //Refresh
            
      
            
            var posOptions = {timeout: 9000, enableHighAccuracy: true};
            
            
            /////////////////////// Current Location  ////////////////
            $cordovaGeolocation
            .getCurrentPosition(posOptions)
                .then(function (position){
                    CurrentLat = position.coords.latitude;
                    CurrentLng = position.coords.longitude;
                    console.log("Position= "+ $scope.lat+ ":" +$scope.long);
                    // $scope.loadResortList();
                }, function(err){
                    // $ionicPopup.alert({
                    //         title: 'Info',
                    //         template: "Goelocation is not working. Please set again."//JSON.stringify(result.description.error)
                            
                    //         });
                    console.log(err);
                    CurrentLat =48.8017941;
                    CurrentLng =2.5389406;
            });
            // getCurrentLocation();
            
            var currentPosition = new google.maps.LatLng(CurrentLat, CurrentLng);
            $rootScope.currentPosition = currentPosition;
            // 42.894647:129.561548
            console.log("kkk"+currentPosition);
            ///////////////////////////////////////////////////////////
            var geocoder = new google.maps.Geocoder();
            var request = {
                latLng: currentPosition
            };
            geocoder.geocode(request, function (data, status) {
                if(status == google.maps.GeocoderStatus.OK){
                    if(data[0] != null){
                        $rootScope.currentAddress = data[0];
                        console.log(data[0]);
                        console.log(data[1]);
                        console.log(data[2]);
                        console.log(data[3]);
                        // if(data[0].address_components.length<6){
                        //     $scope.active_search = data[0].address_components[2].long_name + ', '+ data[0].address_components[4].long_name;
                        // } else{
                        //     $scope.active_search = data[0].address_components[2].long_name + ', '+ data[0].address_components[4].long_name;
                        // }
                        
                        console.log("Full Address is: " + data[0].formatted_address);
                        console.log("Street_number is: " + data[0].address_components[0].long_name);
                        console.log("Route is: " + data[0].address_components[1].short_name);
                        console.log("Locality, Political is: " + data[0].address_components[2].long_name);
                        console.log("Administrative_area_level_2 is: " + data[0].address_components[3].long_name);
                        console.log("Administrative_area_level_1 is: " + data[0].address_components[4].long_name);
                        console.log("Country is: " + data[0].address_components[5].long_name);
                        
             
                        
                        console.log("address is: " + data[1].formatted_address);
                        console.log("Street_number is: " + data[1].address_components[0].long_name);
                //         console.log("Route is: " + data[1].address_components[1].long_name);
                //         console.log("Locality, Political is: " + data[1].address_components[2].long_name);
                //         console.log("Administrative_area_level_2 is: " + data[1].address_components[3].long_name);
                //   //      console.log("Administrative_area_level_1 is: " + data[1].address_components[4].long_name);
                //         console.log("Country is: " + data[1].address_components[5].long_name);
          
                        
                        // console.log("address is: " + data[2].formatted_address);
                        // console.log("address is: " + data[3].formatted_address);
                        // console.log("address is: " + data[4].formatted_address);
                        // console.log("address is: " + data[5].formatted_address);
                       
                        // console.log("address is: " + data[6].formatted_address);

                      
                    } else {
                        console.log('No address available!');
                    }
                }
            });
            
            
            
        });
        // $rootScope.$on('user:logout', function() {
        //   console.log("KKK"+User.isLoggedIn());
        //         $scope.loadResortList();
            
        // });

 
    })

    .controller('UserCtrl', function ($scope, $rootScope, $cordovaNetwork, $cordovaCamera, $cordovaFile, $ionicActionSheet, $cordovaInAppBrowser, $cordovaFacebook, $localStorage, $location, $cordovaGooglePlus, $ionicHistory, $ionicSideMenuDelegate, $state, $ionicPopup, $ionicLoading, APIService, User) {

        $scope.init = function () {
            //User manage part
            $rootScope.loginType = User.getLoginType();
            $rootScope.loggedin = User.isLoggedIn();
            $scope.user = User.profile();
            $scope.login_data = {
                user_email: '',
                user_password: '',
            };
            $scope.register_data = {
                signup_mode:"",
                user_name:"",
                user_password:"",
                user_email:"",
                user_photo_url:"",
                user_search_text1:"",
                user_search_text2:"",
                user_favor_color:"",
                user_photo_data:""
    
            };
            $scope.forgot_data = {
                user_email:''
            }
            $scope.capture_image = '';

   
            // $location.path("/login");
        }
        /**
         * User actions
         */
        $scope.loginByEmail = function(){

            if(User.isValidInfo($scope.login_data)){
                
                User.login(
                    $scope.login_data,
                    "email",
                    function () {
                        $rootScope.updateUser();
                        $state.go('resort-list');
                        $rootScope.$broadcast('userLoggedIn');
                        
                        $rootScope.flag=1;
                    },
                    function (error) {
                        $ionicPopup.alert({
                            title:"Error Occured",
                            template: error.msg
                        });
                    }
                );
            }else{
                $ionicPopup.alert({
                    title: 'Alert',
                    template: 'Invalid user info!'
                });
            }
        };
        //Register
        $scope.register = function () {
          
            if($scope.register_data.user_password==$scope.register_data.confirm_password){
                console.log("user_photo_data" + $scope.register_data.user_photo_data);
           
           

                User.register(
                    $scope.register_data,
                    
                    "email",
                    function(){
                        // $rootScope.$broadcast('userLoggedIn');
                        $rootScope.updateUser();
                        $state.go('resort-list');
                    },
                    function (error) {
                        $ionicPopup.alert({
                            title:"Error Occured",
                            template: error.msg
                        });
                    }
                );
            }
            else{
                 $ionicPopup.alert({
                    title:"Error Occured",
                    template: "Passwords don't match"
                });
            }
        };
        //Upload Photo
        $scope.uploadAvatar = function () {

            $ionicActionSheet.show({
                titleText: 'Get User avatar image',
                buttons: [
                    { text: '<i class="icon ion-camera"></i> Camera' },
                    { text: '<i class="icon ion-ios-albums"></i> Picture' },
                ],
                //destructiveText: 'Delete',
                cancelText: 'Cancel',
                cancel: function() {
                },
                buttonClicked: function(buttonIndex) {
                    if (buttonIndex == 0) {
                        var options = {
                            destinationType: Camera.DestinationType.DATA_URL,
                            sourceType: Camera.PictureSourceType.CAMERA,
                            quality: 50,
                            targetWidth: 128,
                            targetHeight: 128,
                            allowEdit: true,
                            encodingType: Camera.EncodingType.JPEG,
                            saveToPhotoAlbum: false,
                            popoverOptions: CameraPopoverOptions,
                            correctOrientation: true
                        };
                        $cordovaCamera.getPicture(options).then(function(data) {
                            $ionicLoading.hide();
                            $scope.capture_image = "data:image/jpeg;base64," + data;
                            $scope.register_data.user_photo_data =data;
                        }, function (err) {
                            $ionicLoading.hide();
                        });

                    }else if(buttonIndex === 1){
                        var options = {
                            quality: 50,
                            destinationType: Camera.DestinationType.DATA_URL,
                            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                            allowEdit: false,
                            encodingType: Camera.EncodingType.JPEG,
                            popoverOptions: CameraPopoverOptions,
                            saveToPhotoAlbum: false
                        };
                        $ionicLoading.show({
                            template: 'Loading...'
                        });
                        $cordovaCamera.getPicture(options).then(function(data) {
                            $ionicLoading.hide();
                            $scope.capture_image = "data:image/jpeg;base64," + data;
                            $scope.register_data.user_photo_data = data;

                        }, function (err) {
                            $ionicLoading.hide();
                        });
                    }
                    console.log(data);
                    return true;
                },
                destructiveButtonClicked: function() {
                    return true;
                }
            });
        };
        // forgot password
        $scope.forgot = function () {

            if($scope.forgot_data.user_email){
                User.forgotPassword(
                    $scope.forgot_data,
                    function () {
                        $location.path('/resort-list');
                        
                    }
                  
                );
            }else{
                $ionicPopup.alert({
                    title: 'Info',
                    template: 'Invalid info'
                });
            }
        };

        /**
         * Redirect functions
         */
        // go to login page
        $rootScope.goToLogin = function () {
            $state.go('login');
        };
        // signup
        $rootScope.goToSignup = function () {
            $state.go('register');
        };

        // forgot password
        $rootScope.goToForgot = function () {
            $state.go('forgot');
        };

        // go to profile
        $rootScope.goToProfile = function () {
            $state.go('profile');
        };
         $rootScope.goToHome = function () {
            $location.path('/resort-list');
        };

        /**
         * SOCIAL LOGIN
         * Facebook and Google
         */
        // FB Login
        $scope.fbLogin = function () {

            try{
                $ionicLoading.show({
                    template: 'Login...'
                });
                $cordovaFacebook.getLoginStatus()
                    .then(function(success) {
                        /*
                         { authResponse: {
                         userID: "12345678912345",
                         accessToken: "kgkh3g42kh4g23kh4g2kh34g2kg4k2h4gkh3g4k2h4gk23h4gk2h34gk234gk2h34AndSoOn",
                         session_Key: true,
                         expiresIn: "5183738",
                         sig: "..."
                         },
                         status: "connected"
                         }
                         */
                        if(success.status == "connected"){
                            //Goto main page
                            $ionicLoading.hide();
                            $scope.getFBInfo();
                        }else{
                            $cordovaFacebook.login(["public_profile", "email"])
                                .then(function(success) {
                                    /*
                                     * Get user data here.
                                     * For more, explore the graph api explorer here: https://developers.facebook.com/tools/explorer/
                                     * "me" refers to the user who logged in. Dont confuse it as some hardcoded string variable.
                                     *
                                     */

                                    $ionicLoading.hide();
                                    //To know more available fields go to https://developers.facebook.com/tools/explorer/
                                    $scope.getFBInfo();

                                }, function (error) {
                                    // Facebook returns error message due to which login was cancelled.
                                    // Depending on your platform show the message inside the appropriate UI widget
                                    // For example, show the error message inside a toast notification on Android
                                    $ionicLoading.hide();
                                    $ionicPopup.alert({
                                        title: 'FB Login first Cancelled',
                                        template: error.errorMessage
                                    });
                                });
                        }
                    }, function (error) {
                        // error
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'FB Login second Cancelled',
                            template: error.errorMessage
                        });
                    });

            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'FB Login third Cancelled',
                    template: err.message
                });
            }

        };
        // Link facebook
        $scope.fbConnect = function () {
            //will open in app browser
            var options = {
                location: 'yes',
                clearcache: 'no',
                toolbar: 'yes'
            };
            var fbprofile = $localStorage.fbprofile;
            $cordovaInAppBrowser.open('https://www.facebook.com/'+fbprofile.name, '_blank', options)
                .then(function(event) {
                    // success
                })
                .catch(function(event) {
                    // error
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Facebook load error'
                    });
                });
        };
        $scope.getFBInfo = function () {
            try{
                $ionicLoading.show({template: 'Fetch facebook account...'});
                $cordovaFacebook.api("me?fields=id,name,first_name,last_name,gender,email,birthday,address,about,hometown,locale,location,picture", [])
                    .then(function(result){
                        /*
                         * As an example, we are fetching the user id, user name, and the users profile picture
                         * and assiging it to an object and then we are logging the response.
                         */
                        //$localStorage.fbprofile = JSON.stringify(result);
                        /****
                         * Get user info from our server
                         */
                        var loginData = {
                            user_email: result.email,
                            register_type: 'facebook',
                            logintype_id: result.id
                        };

                        User.login(loginData,
                            'Fb',
                            function(){ // login success
                                $rootScope.updateUser();
                                $location.path("/resort-list");
                            },
                            function (err) { // if invalid user info

                                var avatar = result.picture.data.url || 'https://lh3.googleusercontent.com/-77pHuyg_QFs/AAAAAAAAAAI/AAAAAAAAAAA/miTBg963mEg/W96-H96/photo.jpg';

                                $scope.convertImgToBase64(avatar,function(base64image){
                                    var registerData = {
                                        user_name: result.first_name || 'FirstName',
                                       
                                        user_email: result.email,
                                        user_photo_url: base64image,
                                   
                                        logintype_id: result.id
                                    };
                                                                  
          

                                    
                                },'image/jpg');
                            });

                    }, function(error){
                        // Error message
                        $ionicPopup.alert({
                            title: 'FB Login Cancelled',
                            template: error.errorMessage
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
        // Google Plus Login
        $scope.gplusLogin = function () {
            try{
                $ionicLoading.show({template: 'Login with Google+...'});
                //CLIENT_ID: 833224562116-0vvn98au6v2udovdhmb9u1mrmabrvjj5.apps.googleusercontent.com
                //REVERSED_CLIENT_ID: com.googleusercontent.apps.833224562116-0vvn98au6v2udovdhmb9u1mrmabrvjj5    (correct this)
                //iOS API KEY : AIzaSyDxEXEDt1OZDBJsmyZZcOKcckpTUio_icY
                //$cordovaGooglePlus.login('833224562116-0vvn98au6v2udovdhmb9u1mrmabrvjj5.apps.googleusercontent.com')
                $cordovaGooglePlus.login()
                    .then(
                    function(data){
                        $ionicLoading.hide();
                        //$scope.googleData = JSON.stringify(data, null, 4);
                        /****
                         * Get user info from our server
                         */

                        var loginData = {
                            user_email: data.email,
                            register_type: 'Google',
                            logintype_id: data.userId
                        };

                        User.login(loginData,'Google',
                            function () {
                                $rootScope.updateUser();
                                $location.path("/resort-list");
                            },
                            function () {
                                if(!data.imageUrl){
                                    data.imageUrl = 'https://lh3.googleusercontent.com/-77pHuyg_QFs/AAAAAAAAAAI/AAAAAAAAAAA/miTBg963mEg/W96-H96/photo.jpg';
                                }
                                $scope.convertImgToBase64(data.imageUrl,function(base64image){
                                    var registerData = {
                                        user_firstname: data.givenName || 'FirstName',
                                        user_lastname: data.familyName || 'LastName',
                                        user_email: data.email,
                                        user_image: base64image,
                                        register_type:'Google',
                                        
                                        
                                        logintype_id: data.userId
                                    };
                                    User.register(
                                        registerData,
                                        'Google',
                                        function () {
                                            $rootScope.updateUser();
                                            $location.path("/resort-list");
                                        },
                                        function (error) {
                                            $ionicPopup.alert({
                                                title: 'Google+ Info',
                                                template: 'Register failed'
                                            });
                                        });
                                },'image/jpg');
                            }
                        );

                    },
                    function(error){
                        $ionicLoading.hide();

                        // Google returns error message due to which login was cancelled.
                        // Depending on your platform show the message inside the appropriate UI widget
                        // For example, show the error message inside a toast notification on Android

                        $ionicPopup.alert({
                            title: 'Google+',
                            template: JSON.stringify(error)
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Google+',
                    template: err.message
                });
            }
        };

        $scope.convertImgToBase64 = function(url, callback, outputFormat){
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function(){
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this,0,0);
                var dataURL = canvas.toDataURL(outputFormat || 'image/png').replace('data:image/png;base64,','');
                callback(dataURL);
                canvas = null;
            };
            img.src = url;
        }

        $scope.$on('$ionicView.afterEnter', function() {
         
            $rootScope.loggedin = User.isLoggedIn();
            if($rootScope.loggedin){

                if($rootScope.loginType){
                    var user = User.profile();
                    $scope.login_data = {
                        user_email: user.user_email,
                        user_password: user.user_password,
                
                    };
                    $state.go("resort-list");
         
                }
            }
            console.log($ionicHistory.viewHistory().views);
        });
        $scope.$on('$ionicView.enter', function(){
            $ionicSideMenuDelegate.canDragContent(false);
        });
        $scope.$on('$ionicView.leave', function(){
            $ionicSideMenuDelegate.canDragContent(true);
        });
        $scope.$on('user:logout', function(data) {
            $scope.user = User.profile();
            $rootScope.loggedin = User.isLoggedIn();
            $rootScope.loginType = User.getLoginType();
            $rootScope.updateUser();
        });

        $ionicHistory.nextViewOptions({
            //disableAnimate: true,
            disableBack: true
        });
        
        
    })
    .controller('ContactCtrl', function ($scope, $state, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService) {
        $scope.subjectList = [
            {
                id: 0,
                name:'Location'
             
            },
            {
                id: 1,
                name:'Label'
           
            },
            {
                id: 2,
                name:'Business Type'
            
            },
            {
                id: 3,
                name:'Community'
             
            }
        ];
        $scope.user = {
            name:'',
         
            email:'',
          
            message:'',
         
        };
        $scope.contactus = function () {
            $ionicLoading.show({template:'Contacting...'});
            APIService.contactUs(
                $scope.user,
                function (succ) {
                    $ionicLoading.hide();
                    // Do success
                    if(!succ.description.error){
                        $ionicPopup.alert({
                            title:succ.message,
                            template: succ.description
                        });
                    }else{
                        var msg = '';
                        for(var i in succ.description.error){
                            msg += i + ' : ' + succ.description.error[i] + '<br/>'
                        }
                        $ionicPopup.alert({
                            title:succ.message,
                            template: msg
                        });
                    }

                }, function (err) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title:'Info',
                        template: 'This is not allowed in Local Service'//JSON.stringify(err)
                    });
                }
            );
        };
        $scope.goBack=function(){
            $state.go('resort-list');
        };
           
    })

    .controller('ProfileCtrl', function ($scope, $cordovaSocialSharing, $rootScope, $localStorage, $cordovaCamera, $ionicActionSheet, $cordovaInAppBrowser, $cordovaFacebook, $cordovaGooglePlus, $ionicHistory, $state, $ionicPopup, $ionicLoading, APIService, User) {
        
        $scope.init = function () {
            $scope.user=[];
            $scope.user = User.profile();
            $scope.loggedin = User.isLoggedIn();
            $scope.capture_image = '';
            console.log(User.isLoggedIn());
            console.log(User.profile());
            console.log($scope.user);
            
        }
         $scope.goToHome = function () {
            $state.go('resort-list');
        };
        /**
         * User actions
         */
        //Profile update
        $scope.update = function () {
           
            var profile = User.profile();
            var update_data = {
                user_id: $scope.user.user_id,
                user_name:$scope.user.user_name,            
                user_email:$scope.user.user_email,           
                signup_mode:$scope.user.signup_mode,                        
             
                search_text1:$scope.user.search_text1,
                search_text2:$scope.user.search_text2,
                favorit_color:$scope.user.search_color,
                user_photo_data:""
            };
          
            if(($scope.user.user_pass!=$scope.user.user_confirm)||(!$scope.user.user_pass)){
                $ionicPopup.alert({
                    title: 'Error',
                    template: " These passwords don't match "//JSON.stringify(err)
                });
            }else{
                if($scope.user.user_pass){
                    update_data.user_password = $scope.user.user_pass;
                }
                console.log("dff");
                console.log(update_data.user_password);
                if($scope.capture_image){
                    update_data.user_photo_data = $scope.capture_image;
                    $scope.userEdit(update_data);
                }else if($scope.user.user_photo_data){
                    try{
                        console.log("11111");
                        $rootScope.convertImgToBase64($scope.user.user_photo_data,function(base64image){
                            // update_data.user_photo_data = base64image;
                            $scope.userEdit(update_data);
                        })
                    }catch (err){
                        //console.log(err.message);
                    }
                }else{
                    console.log("2222");
                    $scope.userEdit(update_data);
                }
                 
            }
           

        };
        $scope.shareFacebook = function(){
            try{
                $ionicLoading.show({template:'Find Facebook..'});
                $cordovaSocialSharing.shareViaFacebook("AFRISIA",null,null)
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
        $scope.shareTwitter = function(){
           try{
                $ionicLoading.show({template:'Find SNS..'});
                $cordovaSocialSharing
                .shareViaTwitter("This is a Test!",null, null)
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
        $scope.shareLinkedin = function(){
            try{
                $ionicLoading.show({template:'Find SNS..'});
                $cordovaSocialSharing
                .shareVia('linkedin',"This is a Test!", null, null, "Afrisia Is Best!")
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
   
        $scope.userEdit = function (update_data) {
            $ionicLoading.show({
                template: 'Updating...'
            });
                
            APIService.editUser( //if user logged in by emil,then update user info
                update_data,
                function(result){
                    $ionicLoading.hide();
             
                    if(result.status==1){
                        $scope.user = result.current_user;
                        $scope.user.user_password = '';
                        $localStorage.profile = JSON.stringify(result.current_user);
                        $rootScope.updateUser();
                        $ionicPopup.alert({
                            title: 'Info',
                            template: "Update Success!"
                        }).then($state.go('resort-list'));
                       
                    }else{
                        $ionicPopup.alert({
                            title: 'Info',
                            template: result.msg
                        });
                        
                    }
                    
                },
                function (err) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Update error'//JSON.stringify(err)
                    });
                });
                 console.log("dff");
            console.log($localStorage.profile);
        };
        //Login
        $scope.goLogin = function () {
            $rootScope.goToLogin();
        };
        // Logout user
        $scope.logout = function () {
           

            //User related
            User.logout();
            $rootScope.updateUser();
            $rootScope.$broadcast('user:logout',User.profile());
            $rootScope.flag=1;
            $rootScope.researchList();
       

            $ionicLoading.show('Logout..');
            setTimeout(function () {
                $ionicLoading.hide();
                $state.go('resort-list');
            },1000);

        };
        /**
         * Redirect functions
         */
        // Link facebook
        $scope.fbConnect = function () {
            //will open in app browser
            var options = {
                location: 'yes',
                clearcache: 'no',
                toolbar: 'yes'
            };
            $cordovaInAppBrowser.open('https://www.facebook.com/'+$scope.user.user_firstname, '_blank', options)
                .then(function(event) {
                    // success
                })
                .catch(function(event) {
                    // error
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Facebook load error'
                    });
                });
        };
        //Edit profile
        $scope.editAvatar = function () {

            $ionicActionSheet.show({
                titleText: 'Get User avatar image',
                buttons: [
                    { text: '<i class="icon ion-camera"></i> Camera' },
                    { text: '<i class="icon ion-ios-albums"></i> Picture' },
                ],
                //destructiveText: 'Delete',
                cancelText: 'Cancel',
                cancel: function() {
                    //console.log('CANCELLED');
                },
                buttonClicked: function(buttonIndex) {

                    if (buttonIndex == 0) {
                        var options = {
                            destinationType: Camera.DestinationType.DATA_URL,
                            sourceType: Camera.PictureSourceType.CAMERA,
                            quality: 75,
                            targetWidth: 512,
                            targetHeight: 512,
                            allowEdit: true,
                            encodingType: Camera.EncodingType.JPEG,
                            saveToPhotoAlbum: false,
                            popoverOptions: CameraPopoverOptions,
                            correctOrientation: true
                        };

                        $cordovaCamera.getPicture(options).then(function(data) {
                            $scope.capture_image = data;
                            $scope.user.capture1_image="data:image/jpeg;base64," + data;
                           
                        }, function (err) {
                            $ionicLoading.hide();
                        });
                    }else if(buttonIndex == 1){

                        var options = {
                            quality: 50,
                            destinationType: Camera.DestinationType.DATA_URL,
                            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                            allowEdit: false,
                            encodingType: Camera.EncodingType.JPEG,
                            popoverOptions: CameraPopoverOptions,
                            saveToPhotoAlbum: false
                        };
                        $ionicLoading.show({
                            template: 'Loading...'
                        });
                        $cordovaCamera.getPicture(options).then(function(data) {
                            $ionicLoading.hide();
                            $scope.capture_image = data;
                            $scope.user.capture1_image="data:image/jpeg;base64," + data;
                            // $scope.update();
                        }, function (err) {
                            $ionicLoading.hide();
                        });
                    }
                    return true;
                },
                destructiveButtonClicked: function() {
                    return true;
                }
            });
        };

        /**
         * InAppBrowser events listener
         */

        $rootScope.$on('$cordovaInAppBrowser:loadstart', function(e, event){
        });

        $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event){
            // insert CSS via code / file
            $cordovaInAppBrowser.insertCSS({
                code: 'body {background-color:red !important;}'
            });

            //insert Javascript via code / file
            //$cordovaInAppBrowser.executeScript({
            //    file: 'inappbrowser-script.js'
            //});
        });

        $rootScope.$on('$cordovaInAppBrowser:loaderror', function(e, event){
            //alert('$cordovaInAppBrowser:loaderror');
        });

        $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event){
            //alert('$cordovaInAppBrowser:exit');
        });

        $scope.$on('$ionicView.afterEnter', function() {
            $rootScope.loggedin = User.isLoggedIn();
            $scope.user = User.profile();
            $scope.user.user_password = '';
        });

        $scope.$on('user:logout', function(data) {
            $rootScope.updateUser();
            $scope.user = User.profile();
            $rootScope.loggedin = User.isLoggedIn();
            $rootScope.loginType = User.getLoginType();
        });

        //Base64 encode function
        $rootScope.convertImgToBase64 = function(url, callback, outputFormat){
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function(){
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this,0,0);
                var dataURL = canvas.toDataURL(outputFormat || 'image/png').replace('data:image/png;base64,','');
                callback(dataURL);
                canvas = null;
            };
            img.src = url;
        }

    })
   .controller('AboutCtrl', function ($scope, $state, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService) {
        $scope.goBack = function () {
            $state.go('resort-list');
        };
       
       
   })
   .controller('ResearchCtrl', function ($scope, $rootScope, $state, $cordovaSocialSharing, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService, User) {
      $scope.label = {};
       

        $scope.init = function () {
             $rootScope.locationFlag = 0;
             searchCenterLat = CurrentLat;
             searchCenterLng = CurrentLng;
             
             
            $scope.label = {
                user_id: "",
                business_name: "",
                business_type: "",
                business_type_name:"",
                business_address:"",
                business_location: "",
                search_text: "",
                search_text0:"",
                search_text1:"",
                search_text2:"",
                search_text3:"",
                search_text4:"",
                labelList : "",
                flag0: 0,
                flag1: 0,
                flag2: 0,
                flag3: 0,
                flag4: 0,
                flag5: 0,
                flag6: 0,
                flag7: 0,
                num:0
                
            }
            $scope.label.business_type1 ={
                type_id:null,
                business_type:null
            }
          
            // $scope.label.typeList = [
            //     {
            //         id: 0,
            //         name:'Restaurant'      
            //     },
            //     {
            //         id: 1,
            //         name:'librairie'
            
            //     },
            //     {
            //         id: 2,
            //         name:'Bar'
                
            //     },
            //     {
            //         id: 3,
            //         name:'Night Club'
                
            //     }
            // ];
          
            //Refresh
             
            if(User.isLoggedIn()){
                var user=User.profile();
                $scope.label.user_id = user.user_id;
                console.log("1:::"+$scope.label.user_id);
            }else{
                $scope.label.user_id="";
                console.log("0:::"+$scope.label.user_id);
            }
            $scope.label.labelList = JSON.parse($localStorage.labelList);
            console.log($scope.label.labelList);
           
            $scope.label.typeList = JSON.parse($localStorage.bussinessType);
            console.log("Type::::"+$scope.label.typeList);
            $state.go($state.current, {}, {reload: true});
     
              
        }
        $scope.addSearch = function(){
            $scope.label.num++;
            console.log( $scope.label.num);
            
        };
        $scope.initialize = function() {
            $scope.getAutoLocation();
        };
        $scope.getAutoLocation = function(){
             // Create the autocomplete object, restricting the search to geographical location types.         
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {HTMLInputElement} */(document.getElementById('inputAddressBar')),
                { types: [] });
            // When the user selects an address from the dropdown,populate the address fields in the form.
            //  alert("initializes");
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                isEnteredAddress = true;
                $scope.fillInAddress(autocomplete.getPlace());
                // alert("selected");
            });
        }
        $scope.fillInAddress = function(temp_place){
            // Get the place details from the autocomplete object.
            globalAddressObject = temp_place;
  //      console.log("Selected Address from drop down menu is :"+temp_place.address_components[0]);
            // stops execution if address is not selected from the drop down menu
            globalMarkerLocation = temp_place.geometry.location;
            console.log("selected lat lng: "+globalMarkerLocation.lng());
            // refreshMap(globalMarkerLocation,globalRadius);
            searchCenterLat = globalMarkerLocation.lat();
            searchCenterLng = globalMarkerLocation.lng();
            
            
            geofenceMap = null;
            
        }
        $scope.searchLabel0 = function(item){
            console.log(item.label_name);
            $scope.label.search_text0 = item.label_name;
            $scope.label.flag0=1;
        }
        $scope.flag0Change = function (){
            console.log("change");
            $scope.label.flag0=0;
        }
        $scope.searchLabel1 = function(item){
            console.log(item.label_name);
            $scope.label.search_text1 = item.label_name;
            $scope.label.flag1=1;
        }
        $scope.flag1Change = function (){
            console.log("change");
            $scope.label.flag1=0;
        }
        $scope.searchLabel2 = function(item){
            console.log(item.label_name);
            $scope.label.search_text2 = item.label_name;
            $scope.label.flag2=1;
        }
        $scope.flag2Change = function (){
            console.log("change");
            $scope.label.flag2=0;
        }
        $scope.searchLabel3 = function(item){
            console.log(item.label_name);
            $scope.label.search_text3 = item.label_name;
            $scope.label.flag3=1;
        }
        $scope.flag3Change = function (){
            console.log("change");
            $scope.label.flag3=0;
        }
        $scope.searchLabel4 = function(item){
            console.log(item.label_name);
            $scope.label.search_text4 = item.label_name;
            $scope.label.flag4=1;
        }
        $scope.flag4Change = function (){
            console.log("change");
            $scope.label.flag4=0;
        }

     
        $scope.goBack = function () {
             $scope.label.num=0;
            $state.go('resort-list',{},{reload: true});
        };
        $scope.goSearchResult = function () {
            console.log("SearchCenter Lat ="+ searchCenterLat);
            console.log("SearchCenter Lng ="+ searchCenterLng);
           console.log($scope.label.business_type1);
            $scope.label.business_type=$scope.label.business_type1.type_id;
             $scope.label.business_type_name=$scope.label.business_type1.business_type;
             $scope.label.num=0;
             console.log($scope.label);
             
             if($scope.label.business_location){
                 $rootScope.locationFlag = 1;
             }else{
                 $rootScope.locationFlag = 0;
             }
             
             
             
             
        
            $scope.label.search_text="";
            if($scope.label.search_text0!=""){
                if($scope.label.search_text==""){
                    $scope.label.search_text=$scope.label.search_text0;
                }
                else{
                    $scope.label.search_text=$scope.label.search_text+'@'+$scope.label.search_text0;
                }
                
            }
            if($scope.label.search_text1!=""){
                if($scope.label.search_text==""){
                    $scope.label.search_text=$scope.label.search_text1;
                }
                else{
                    $scope.label.search_text=$scope.label.search_text+'@'+$scope.label.search_text1;
                }
                
            }
            if($scope.label.search_text2!=""){
                if($scope.label.search_text==""){
                    $scope.label.search_text=$scope.label.search_text2;
                }
                else{
                    $scope.label.search_text=$scope.label.search_text+'@'+$scope.label.search_text2;
                }
                
            }
            if($scope.label.search_text3!=""){
                if($scope.label.search_text==""){
                    $scope.label.search_text=$scope.label.search_text3;
                }
                else{
                    $scope.label.search_text=$scope.label.search_text+'@'+$scope.label.search_text3;
                }
                
            }
            if($scope.label.search_text4!=""){
                if($scope.label.search_text==""){
                    $scope.label.search_text=$scope.label.search_text4;
                }
                else{
                    $scope.label.search_text=$scope.label.search_text+'@'+$scope.label.search_text4;
                }
                
            }
            $scope.label.search_text0 = "";
            $scope.label.search_text1 = "";
            $scope.label.search_text2 = "";
            $scope.label.search_text3 = "";
            $scope.label.search_text4 = "";
            
            
            //save active label details
            console.log($scope.label);
            $localStorage.advance_label=JSON.stringify($scope.label);
            $scope.label.search_text = "";
     
           
           
            $rootScope.flag=1;
           
         
            // $rootScope.$apply();
            console.log("go");
            console.log($rootScope.flag);
          
            $state.go('search-result',{},{reaload:true} );
          
        };
       
       
   })
//    .controller('MyInstallCtl', function ($scope, $state) {
//         $scope.goBack = function () {
//             $state.go('resort-list');
//         };
//         $scope.goSearchResult = function () {
//             $state.go('search-result');
//         };
       
       
//    })
    .controller('MyInstallCtl', function ($http, $rootScope, $scope, $state, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService, User) {
        $scope.init = function(){
            var profile = User.profile();
            $scope.loggedin=User.isLoggedIn();
            console.log(profile.user_id);
            var comment={
                user_id: profile.user_id,
                category: "1" 
            };
            $ionicLoading.show({
                    template: 'Loading...'
                });
                    
                APIService.loadMine( //if user logged in by emil,then update user info
                    comment,
                    function(result){
                        console.log(result);
                        $ionicLoading.hide();
                        
                        if(result.status==1){
                            $scope.items = result.category_item;
                            
                            
                        }
                    },
                    function (err) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Error',
                            template: 'Loading error'//JSON.stringify(err)
                        });
                });

                
            };
            // $http.get('js/data.json').success(function(data){
            //      $scope.artists=data;
            // });
            
      
        $scope.goBack=function(){
                $state.go('resort-list');
        };
        $scope.goEdit_Install=function(){
                $state.go('addinstitution');
        };
        $scope.$on('$ionicView.afterEnter', function() {
            console.log("fsfdasdas"+$rootScope.flag);
            if($rootScope.addBusiness_flag==1){
                $scope.init();
                $rootScope.addBusiness_flag=0;
            }             //Refresh
            
        });
        
        
   
           
    })
    .controller('AddOpinionCtl', function ($http, $rootScope, $cordovaSocialSharing, $scope, $state, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService, ModalService, User) {
         $scope.$on('$ionicView.afterEnter', function() {
            console.log("fsfdasdas"+$rootScope.addComment_flag);
            if($rootScope.addComment_flag==1){
                $scope.init();
                $rootScope.addComment_flag=0;
            }             //Refresh
            
        });
        $scope.$on('$ionicView.afterEnter', function() {
 
                $scope.init();
          
                        //Refresh
            
        });
        $scope.goLabelChoose = function(index){
            $scope.selectedLabel = index;
            console.log(index);
            if($scope.labels[index].vote_num<1){
            ModalService
            .init('templates/cvv.html', $scope)
            .then(function (modal) {
                modal.show();
            });
            }
        }
        
        $scope.labelList = [
            { text: "Public", value: "5"},
            { text: "Private", value: "6"},
            { text: "Secret", value: "7"}
        ];
        $scope.labelChange = function(item){
            console.log("Label value"+item.value);
            console.log($scope.labels[$scope.selectedLabel]);
            if($scope.labels[$scope.selectedLabel].vote_num < 1){
                $scope.labels[$scope.selectedLabel].rate = item.value
            }
            console.log($scope.labels[$scope.selectedLabel]);
        }

        $scope.pointList = [
        //     // {
        //     //     id: 10,
           
        //     // },
        //     // {
        //     //     id: 9,
               
        //     // },
        //     // {
        //     //     id: 8,
          
        //     // },
        //     // {
        //     //     id: 7,
               
        //     // },
        //     // {
        //     //     id: 6,
               
        //     // },

            {
                id: 5,
               
            },
            {
                id: 4,
               
            },
            {
                id: 3,
               
            },
            {
                id: 2,
               
            },
            {
                id: 1,
               
            },
            {
                id: 0,
               
            }
        ];
  
        $scope.label = [
            { name: null ,
              title: null,
              content: null,
              rate:null
             
            }
        ]
        var user=User.profile();
        console.log("USER_ID:"+user.user_id);
 
        $scope.init = function(){
             $scope.label = [
                {
                    name: null ,
                    title: null,
                    content: null,
                    rate:null
                
                }
            ]
            $scope.comment_data = 
                {
                    user_id: "",
                    business_id:"",
                    title: "",
                    content: "",
                    rate_score: "",
                    label:""
                };
            
            $scope.labels = [
                {
                     name: "" ,
                     vote_num: "",
                   
                     point: "",
                     title: "",
                     content: "",
                     rate:""
                }               
            ];
         
            if($localStorage.active_business){
                var active_business=JSON.parse($localStorage.active_business);
                $scope.active_business=active_business;
                console.log(active_business);
            }
            for(var i=0; i<$scope.active_business.label_detail.length; i++){
            
                $scope.labels.push({ name: "", point: "", title: "", content:"", rate: "", vote_num: "" });
                $scope.labels[i].name=$scope.active_business.label_detail[i].label_name;
                $scope.labels[i].vote_num=$scope.active_business.label_detail[i].vote_num;
                $scope.labels[i].rate=$scope.active_business.label_detail[i].avg_rate;
                console.log(i+":"+$scope.labels[i].name);
            }
        }
     
        console.log("COMMENT:"+$scope.comment_data);
        // $scope.labelOption = function (label) {
      
        //    console.log(label.point.id);
        // }
        
        $scope.addComment = function(){
            var labelRateFlag = 1;
            for(var i=0;i< $scope.labels.length-1;i++){
             
               console.log($scope.labels[i].point.id);
                    if(!$scope.labels[i].point.id){
                        labelRateFlag = 0;
                        
                    }
       

                }
                 console.log(labelRateFlag);
            if($scope.rate&&$scope.title&&$scope.content&&(labelRateFlag>0)&&($scope.labels.length>1)){   
                  console.log("$scope.labels.length="+$scope.labels.length);
                for(var i=0;i< $scope.labels.length-1;i++){
                    if(i<$scope.labels.length-2){
                    $scope.label.push({ name: "", rate: "", title: "", content:"" });
                    }
                    if($scope.labels[i].rate>5){
                         $scope.label[i].rate=$scope.labels[i].rate;
                    } else{
                        $scope.label[i].rate=$scope.labels[i].point.id;
                    }
                  
                    $scope.label[i].name=$scope.labels[i].name;
                    $scope.label[i].title=$scope.labels[i].title;
                    $scope.label[i].content=$scope.labels[i].content;
                    console.log($scope.label[i].name);
                }
                
                $scope.comment_data.user_id= user.user_id;
                $scope.comment_data.business_id=$scope.active_business.business_id;
                $scope.comment_data.title=$scope.title;
                $scope.comment_data.content=$scope.content;
                $scope.comment_data.rate_score=$scope.rate.id;
                // $localStorage.comment_data_label=JSON.stringify($scope.label);
                // $scope.comment_data.label=JSON.parse($localStorage.comment_data_label);
                $scope.comment_data.label=$scope.label;
                console.log("comment_data.user_id:::::::"+$scope.comment_data.user_id);
                console.log("comment_data.business_id:::"+$scope.comment_data.business_id);
                console.log("comment_data.title:::::::::"+$scope.comment_data.title);
                console.log("comment_data.desc::::::::::"+$scope.comment_data.desc);
                console.log("comment_data.label:::::::::"+$scope.comment_data.label);
                console.log("comment_data.rate::::::::::"+$scope.comment_data.label[0].title);
                console.log("comment_data.rate::::::::::"+$scope.comment_data.rate);
                
                
                 $scope.uploadingComment($scope.comment_data);
                 $scope.labels = [
                    { name: "" ,
                    point: "",
                    title: "",
                    content: "",
                    rate:""
                    }               
                ];
                $scope.label = [
                    { name: "" ,
                    title: "",
                    content: "",
                    rate:""
                    
                    }
                ];
                for(var i=0; i<$scope.active_business.label_detail.length; i++){
            
                    $scope.labels.push({ name: "", point: "", title: "", content:"" });
                    $scope.labels[i].name=$scope.active_business.label_detail[i].label_name;
                    console.log(i+":"+$scope.labels[i].name);
                }
               
                $rootScope.$broadcast('reLogin');
                $rootScope.flag=1;
                $rootScope.addComment_flag=1;
                console.log("detail");
                $state.go("resort-list");
                
            }else{
               $ionicPopup.alert({
                            title: 'Info',
                            template: "Please fill in the blankBox."    
               });
            }
            
        }
        $scope.uploadingComment = function(temp){
            console.log(temp);
            APIService.addComment(
            temp,
            function(result){
               
                if(result.status){
                    if(result.result){
                        console.log(result.msg); 
                        $state.go("resort-list");
                        $rootScope.$broadcast('reLogin'); 
                                 
                    }else{
                        $ionicPopup.alert({
                            title: 'Info',
                            template: result.msg    
                        });
                       
                    }   
                 }else{
                    $ionicPopup.alert({
                        title: 'Info',
                        template: "Fill in the blank text correctly"//JSON.stringify(result.description.error)
                    });
                    
                }                        
            },
            function (err) {
        
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'Server connect error'//JSON.stringify(err)
                });
            });
        }

        $scope.addLabel = function () {
            if($scope.labels[$scope.labels.length-1].name){
           
     
               console.log($scope.title);
             if($scope.rate){
               console.log("sdadasd"+$scope.rate.id);
             }
                $scope.labels.push({ name: "", point: "", title: "", content:"", rate: "", vote_num: "" });
                
                
               
               
                console.log($scope.labels);
                console.log("THis"+$scope.label[0].name);
            }else{
                 $ionicPopup.alert({
                    title: 'Alert',
                    template: "Write in the Label Name."
                });
            }
        }

        // $scope.removeLabel = function (index) {
        //     $scope.labels.splice(index, 1);
        // }
        


        $scope.label_num=0;
        console.log($scope.label_num);
        $scope.goBack=function(){
            $state.go('comment-view');
        };
        // $scope.addLabel=function(){
        //     $scope.label_num++;
        //     console.log($scope.label_num);
        //     console.log($scope.title);
        //     console.log($scope.content);
        // };
 
 
         $scope.loadBusiness= function(callback){
    
         
            if($localStorage.active_business){
                var active_business=JSON.parse($localStorage.active_business);
                $scope.active_business=active_business;
                console.log(active_business);
            }
            
        };



        $scope.shareFacebook = function(){
            try{
                $ionicLoading.show({template:'Loading Facebook...'});
                $cordovaSocialSharing.shareViaFacebook("AFRISIA",null,null)
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
        $scope.shareTwitter = function(){
           try{
                $ionicLoading.show({template:'Loading Twitter...'});
                $cordovaSocialSharing
                .shareViaTwitter($scope.title,null, null, $scope.content)
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
        $scope.shareLinkedin = function(){
            try{
                $ionicLoading.show({template:'Loading Linkedin...'});
                $cordovaSocialSharing
                .shareVia('linkedin',$scope.title, null, null, $scope.content)
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
      
    //     $scope.addComment=function(){
            
    //       $ionicLoading.show({
    //             template: 'Loading...'
    //         });
    //         APIService.addCommentApi( //if user logged in by emil,then update user info
    //             add_Comment,
    //             function(result){
    //                 console.log(result);
    //                 $ionicLoading.hide();
                  
    //                 if(result.status==1){
                        
    //                     console.log(User.isLoggedin);
                     
                      
    //                 }
    //             },
    //             function (err) {
    //                 $ionicLoading.hide();
    //                 $ionicPopup.alert({
    //                     title: 'Error',
    //                     template: 'Loading error'//JSON.stringify(err)
    //                 });
    //             });
    //             $state.go('leave-comment');
     
    //      };
    
        
   
           
    })
    .controller('MyFavoritCtl', function ($http, $scope, $rootScope, $state, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService, User) {
    
        $scope.goBack=function(){
            $state.go('resort-list');
        };
        $scope.init = function(){
            var profile = User.profile();
            $scope.loggedin=User.isLoggedIn();
            console.log(profile.user_id);
            var comment={
            user_id: profile.user_id,
            category: "3" 
            };
            $ionicLoading.show({
                    template: 'Loading...'
            });
                    
            APIService.loadMine( //if user logged in by emil,then update user info
                comment,
                function(result){
                    console.log(result);
                    $ionicLoading.hide();
                    
                    if(result.status==1){
                        $scope.items = result.category_item;
                        
                        
                    }
                },
                function (err) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Loading error'//JSON.stringify(err)
                    });
            });
        }
        $scope.$on('$ionicView.afterEnter', function() {
            console.log("fsfdasdas"+$rootScope.flag);
            if($rootScope.favor_flag==1){
                $scope.init();
                $rootScope.favor_flag=0;
            }             //Refresh
            
        });
     

   
           
    })
     .controller('MyCommentCtl', function ($http, $rootScope, $scope, $state, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService, User) {
         $scope.init = function(){
    
            var profile = User.profile();
            $scope.user_name=profile.user_name;
            $scope.user_photo=profile.user_photo_url;
            $scope.loggedin=User.isLoggedIn();
            console.log(profile);
            var comment={
                user_id: profile.user_id,
                category: "2" 
            };
            $ionicLoading.show({
                    template: 'Loading...'
                });
                    
                APIService.loadMine( //if user logged in by emil,then update user info
                    comment,
                    function(result){
                        console.log(result);
                        $ionicLoading.hide();
                    
                        if(result.status==1){
                            $scope.items = result.category_item;
                        
                        
                        }
                    },
                    function (err) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Error',
                            template: 'Loading error'//JSON.stringify(err)
                    });
            });
         }
        $scope.goBack=function(){
            $state.go('resort-list');
        };
     
        $scope.goEdit_Install=function(){
            $state.go('addinstitution');
        };
        $scope.$on('$ionicView.afterEnter', function() {
            console.log("fsfdasdas"+$rootScope.flag);
            if($rootScope.addComment_flag==1){
                $scope.init();
                $rootScope.addComment_flag=0;
            }             //Refresh
            
        });
   
           
    })

   .controller('SearchResultCtl', function($scope,$ionicScrollDelegate, $rootScope, $state, $cordovaSocialSharing, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService, User){
         $scope.init = function () {
            $scope.label = {
                user_id: "",
                business_name: "",
                business_type: "",
                business_type_name:"",
                business_address:"",
                search_text: "",
              
                
            }
       
            $scope.data = {
                showDelete: false
            };
            $scope.items=[];
            // $scope.loadSearchResult();
            $rootScope.active_business = null;
            if($localStorage.advance_label){
               var label=JSON.parse($localStorage.advance_label);
               $scope.label.user_id=label.user_id;
               $scope.label.business_name=label.business_name;
               $scope.label.business_type=label.business_type;
               $scope.label.business_type_name=label.business_type_name;
               $scope.label.business_address=label.business_address;
               $scope.label.search_text=label.search_text;
               console.log($scope.label);
        
           }
      
       
        }
        $scope.changeFavor = function(item){
            $rootScope.flag=1;
            $rootScope.favor_flag=1;
            var user = User.profile();
            if(item.favor_status==1){
                
                var temp = { 
                    business_id: item.business_id,
                    user_id: user.user_id,
                    favor: "0"
                }
                console.log(user);
                console.log("iiiiiif"+temp);
                APIService.changeFavorit(
                        
                    temp,
                    function(result){
                        if(result.status){
                            
                                $ionicPopup.alert({
                                    title: 'Info',
                                    template: "Changed Successfully"//JSON.stringify(result.description.error)
                                });
                                item.favor_status=0;
                                $rootScope.$broadcast('reLogin');
                            }
                            else{
                                $ionicPopup.alert({
                                    title: 'Info',
                                    template: "You must user login."//JSON.stringify(result.description.error)
                                });                        
                            }                          
                                                    
                    },
                    function (err) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Error',
                            template: 'Server connect error'//JSON.stringify(err)
                        });

                        
                    });
            }else{
                
                
                temp = { 
                    business_id: item.business_id,
                    user_id: user.user_id,
                    favor:"1"
                }
                console.log(user);
                console.log("sdfdsf"+temp);
                APIService.changeFavorit(
                        
                    temp,
                    function(result){
                        if(result.status){
                            
                                $ionicPopup.alert({
                                    title: 'Info',
                                    template: "Changed Successfully"//JSON.stringify(result.description.error)
                                });
                                item.favor_status=1;
                                $rootScope.$broadcast('reLogin');
                            }
                            else{
                                $ionicPopup.alert({
                                    title: 'Info',
                                    template: "You must login."//JSON.stringify(result.description.error)
                                });                        
                            }                          
                                                    
                    },
                    function (err) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Error',
                            template: 'Server connect error'//JSON.stringify(err)
                        });

                        
                    });
            }
            
            
            
        }
        //   $scope.share = function(item) {
        //     console.log(item);

        //     var resort = item;
        //     console.log(resort);
        //     try{
        //         $ionicLoading.show({template:'Find SNS..'});
        //         $cordovaSocialSharing.share(resort.business_name, "AFRISIA", resort.business_photo_url, null)
        //             .then(function(result) {
        //                 $ionicLoading.hide();
        //             }, function(error) {
        //                 $ionicLoading.hide();
        //                 $ionicPopup.alert({
        //                     title:'Error',
        //                     template:'Cannot share'
        //                 });
        //             });
        //     }catch (err){
        //         $ionicLoading.hide();
        //         $ionicPopup.alert({
        //             title: 'Alert',
        //             template: err.message
        //         });
        //     }
        // };
        
           $scope.openBusiness = function(item){

       
                $rootScope.active_business = item;
                //save active resort details
                $localStorage.active_business = JSON.stringify(item);
                $state.go('comment-view');
              
       
     
        }
        $scope.scrollMainToTop = function() {
            $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
        };
       
        $scope.loadSearchResult = function () {
             $scope.$broadcast('scroll.refreshComplete');
            //  $state.go($state.current, {}, {reload: true});
             $scope.scrollMainToTop();
             if($localStorage.advance_label){
               var label=JSON.parse($localStorage.advance_label);
               $scope.label.user_id=label.user_id;
               $scope.label.business_name=label.business_name;
               $scope.label.business_type=label.business_type;
               $scope.label.business_address=label.business_address;
               $scope.label.search_text=label.search_text;
               console.log($scope.label);
        
           }
            
            console.log($scope.label);
            $ionicLoading.show({
                template: 'Loading...'
            });
            $scope.items = [
                
            ];
            console.log($scope.label);
            
            APIService.resortList(
                $scope.label,
               function(result){
                    $ionicLoading.hide();
                    if(result.status){
                        if(result.result){
                            
                            console.log(result.result);
                            console.log(result);
                           // $localStorage.forMap=JSON.stringify($scope.items);
                           $scope.getItems(result.result);
                           $localStorage.forMap=JSON.stringify($scope.items);
   
                        
                            
                          
                        }
                        else{
                            $ionicPopup.alert({
                                title: 'Info',
                                template: result.msg//JSON.stringify(result.description.error)
                                
                                
                                
                            });
                            
                         
                        }
                    
                    }else{
                        $ionicPopup.alert({
                            title: 'Info',
                            template: "Fill in the blank text correctly"//JSON.stringify(result.description.error)
                        });
                        
                    }

                   
                },
                function (err) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Server connect error'//JSON.stringify(err)
                    });

                    
                });
               

        };
        $scope.getItems = function(business){
            
            console.log("Business= " + business);
            console.log("$rootScope.locationFlag = " + $rootScope.locationFlag);
            
            if(!$rootScope.locationFlag){
                $scope.items = business;
            }else{
                for(var i=0; i< business.length; i++){
                    currentDistance = getDistanceFromLatLonInKm(searchCenterLat, searchCenterLng, business[i].business_lati,business[i].business_long);
                    if(currentDistance < 50){
                        $scope.items.push(business[i]);
                    }
                }
                
            }
            
        }
        $scope.goBack = function () {
            $state.go('research');
        };
        $scope.goSearchMap = function () {
            $state.go('search-map');
        };
        $scope.goComment = function (item) {
            $state.go('comment-view');
        };
        
        $scope.$on('$ionicView.afterEnter', function() {
            console.log("fsfdasdas"+$rootScope.flag);
            if($rootScope.flag==1){
                $scope.loadSearchResult();
                $rootScope.flag=0;
            }             //Refresh
            
        });
        // $scope.$on('$ionicView.afterLeave',function(){
      
        //      $state.go($state.current, {}, {reload: true});
       
            
        // })
  
   })

   .controller('CommentCtl', function($scope, $rootScope, $state, $cordovaSocialSharing, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService,$ionicViewService, User){
      
        $scope.init = function (){
            $scope.business=[];
            
        };
        $scope.goAddOpinion = function () {
            if(User.isLoggedIn()){
                $state.go('addopinion');
            }else{
                $ionicPopup.alert({
                            title:'Error',
                            template:'You must log in Afrisia to leave comment.'
                });
            }
        };
        $scope.goBack = function () {
            $ionicViewService.getBackView().go();
            // $state.go('resort-list');
        };
        $scope.goSingleMap = function () {
           $state.go('single-map', {}, {reload: true});
        //    $rootScope.$apply();
        };
        $scope.goLabelComment = function (label) {
            if($localStorage.active_business){
                var active_business=JSON.parse($localStorage.active_business);
   
                var active_label = {
                    label: label.label_name,
                    business_id: active_business.business_id
                }
                $localStorage.active_label=JSON.stringify(active_label); 
                console.log(active_label);
                
            }
            
            
            $state.go('label-comment');
        };
        $scope.loadBusiness= function(callback){
    
         
            if($localStorage.active_business){
                var active_business=JSON.parse($localStorage.active_business);
                $scope.active_business=active_business;
                console.log(active_business);
            }
            
        };
        $scope.doRefresh = function () {
            $scope.loadBusiness(function(){
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        $scope.$on('$ionicView.afterEnter', function() {
            $scope.loadBusiness();
        });
        $scope.shareFacebook = function(){
            try{
                $ionicLoading.show({template:'Loading Facebook...'});
                $cordovaSocialSharing.shareViaFacebook("AFRISIA",$scope.active_business.business_photo_url,null)
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
        $scope.shareTwitter = function(){
           try{
                $ionicLoading.show({template:'Loading Twitter...'});
                $cordovaSocialSharing
                .shareViaTwitter($scope.title,$scope.active_business.business_photo_url, $scope.active_business.business_photo_url, $scope.content)
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
        $scope.shareLinkedin = function(){
            try{
                $ionicLoading.show({template:'Loading Linkedin...'});
                $cordovaSocialSharing
                .shareVia('linkedin',$scope.title,  $scope.active_business.business_photo_url, null, $scope.content)
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
  
   })
   .controller('AddInstitutionCtl', function($scope, $rootScope, $localStorage, $cordovaCamera, $ionicActionSheet, $cordovaInAppBrowser, $cordovaFacebook, $cordovaGooglePlus, $ionicHistory, $state, $ionicPopup, $ionicLoading, APIService, User) {
        $scope.init= function(){
            $scope.loggedin=User.isLoggedIn();
            $scope.capture_image="";
            $scope.business={
                user_id: User.profile().user_id,
                business_name: "",
                business_site: "",
                business_address:"" ,
                business_city:"" ,
                business_description:"" ,
                business_photo_data: "",
                business_lati: "",
                business_long: ""
           
       
           };
           $scope.business.business_type1 ={
                type_id:null,
                business_type:null
            }
            
          

            $scope.business.typeList = JSON.parse($localStorage.bussinessType);
            console.log("Type::::"+$scope.business.typeList);
            $state.go($state.current, {}, {reload: true});
            
            $scope.currentPosition_flag = 1;
            console.log("current correct Position="+ $rootScope.currentPosition.lng());
            fillInAddress($rootScope.currentAddress);
            // $scope.business.business_address = $rootScope.currentAddress.address_components[0].long_name +' '+ $rootScope.currentAddress.address_components[1].long_name
            // if($scope.currentAddress.address_components.length<6){
            //     $scope.business.business_city = $rootScope.currentAddress.address_components[2].long_name + ', '+ $rootScope.currentAddress.address_components[4].long_name;
            // } else{
            //     $scope.business.business_city = $rootScope.currentAddress.address_components[2].long_name + ', '+ $rootScope.currentAddress.address_components[4].long_name;
            // }
            
        }
        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'long_name',
            country: 'short_name'
            // postal_code: 'short_name'
        };
        $scope.initialize = function() {
            $scope.currentPosition_flag = 0;
            
            autocomplete = new google.maps.places.Autocomplete(
	      /** @type {HTMLInputElement} */(document.getElementById('address')),
	       { types: ['geocode'] });


       $scope.business.business_city = '';
               $scope.business.business_address = '';
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
     
           google.maps.event.addListener(autocomplete, 'place_changed', function() {
        
                isEnteredAddress = true;
                 fillInAddress(autocomplete.getPlace());
		 // alert("selected");
	       });
        };
         $scope.initialize1 = function() {
             
             $scope.currentPosition_flag = 0;
                       $scope.business.business_city = '';
               $scope.business.business_address = '';            
            autocomplete = new google.maps.places.Autocomplete(
	      /** @type {HTMLInputElement} */(document.getElementById('city')),
	       { types: ['geocode'] });


        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        //    autocomplete.addListener('place_changed', fillInAddress);
           google.maps.event.addListener(autocomplete, 'place_changed', function() {
               console.log(autocomplete.getPlace());
     
                isEnteredAddress = true;
                fillInAddress(autocomplete.getPlace());
		 // alert("selected");
	       });
        };
      
        function fillInAddress(place) {
            // Get the place details from the autocomplete object.
     
            console.log(place);
            console.log(place.geometry.location.lng());
   

            // for (var component in componentForm) {
            //     document.getElementById(component).value = '';
            //     document.getElementById(component).disabled = false;
            // }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            document.getElementById('address').value = '';
            document.getElementById('city').value = '';
            for (var i = 0; i <place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    if(i < 3 ){
                        if(document.getElementById('address').value !=''){
                            document.getElementById('address').value +=', '
                        }
                        
                        document.getElementById('address').value += val;
                    } else{
                        if(document.getElementById('city').value !=''){
                            document.getElementById('city').value +=', '
                        }
                        document.getElementById('city').value += val;
                    }
                    
                    
          
                }
            }
            
     
                $scope.business.business_lati = place.geometry.location.lat();
                $scope.business.business_long = place.geometry.location.lng();
      
  
            $scope.business.business_city = document.getElementById('city').value;
            $scope.business.business_address = document.getElementById('address').value;
        }
        $scope.convertImgToBase64 = function(url, callback, outputFormat){
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function(){
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this,0,0);
                var dataURL = canvas.toDataURL(outputFormat || 'image/png').replace('data:image/png;base64,','');
                callback(dataURL);
                canvas = null;
            };
            img.src = url;
        }
        
        
        $state.go($state.current, {}, {reload: true});

        $scope.add_business= function(){
            
            $ionicLoading.show({
                template: 'Loading...'
            });
            
        //     $scope.business={
        //         user_id: User.profile().user_id,
        //         business_name: $scope.business_name,
        //         business_site: $scope.business_site,
        //         business_address: $scope.business_address,
        //         business_city: $scope.business_city,
        //         business_description: $scope.business_description ,
        //         business_photo_data: $scope.capture_image
           
       
        //    }; 
          var avatar="$scope.business_pic";
          
                    
                                        
            
           $scope.convertImgToBase64(avatar,function(base64image){
                            $scope.business.business_photo_data= base64image;
                                                                  
          

                                    
                                },'image/jpg');
           $scope.business.business_type=$scope.business.business_type1.type_id;
           var business= {
                user_id     : $scope.business.user_id,
                business_photo_data  : $scope.business.business_photo_data,
                business_name   : $scope.business.business_name,
                business_site   : $scope.business.business_site,
                business_address  : $scope.business.business_address,
                business_city   : $scope.business.business_city,
                business_description : $scope.business.business_description,
                business_type   : $scope.business.business_type,
                business_lati : $scope.business.business_lati,
                business_long: $scope.business.business_long

            }
            console.log(business);
            console.log("addd"+$scope.business.business_type);
            APIService.addBusiness( //if user logged in by emil,then update user info
                business,
                function(result){
                    console.log(result);
                    $ionicLoading.hide();
                  
                    if(result.status==1){
                        $rootScope.flag=1;
                        $rootScope.addBusiness_flag=1;
                        $rootScope.$broadcast('reLogin');
                        $ionicPopup.alert({
                            title: 'Congratulation!',
                            template: "Added successfully"//JSON.stringify(result.description.error)
                            
                            
                            
                                
                                
                        }).then($state.go("resort-list", {}, {reload: true}));
                     
                      
                    }else{
                        $ionicPopup.alert({
                            title: 'Info',
                            template: result.msg//JSON.stringify(result.description.error)
                            
                                
                                
                        });
                    }
                },
                function (err) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Uploading error'//JSON.stringify(err)
                    });
                })
        };

   
        $scope.goBack = function () {
            $state.go('myinstall');
        };
      
          //Edit profile
        $scope.editAvatar = function () {

            $ionicActionSheet.show({
                titleText: 'Get User avatar image',
                buttons: [
                    { text: '<i class="icon ion-camera"></i> Camera' },
                    { text: '<i class="icon ion-ios-albums"></i> Picture' },
                ],
                //destructiveText: 'Delete',
                cancelText: 'Cancel',
                cancel: function() {
                    //console.log('CANCELLED');
                },
                buttonClicked: function(buttonIndex) {

                    if (buttonIndex == 0) {
                        var options = {
                            destinationType: Camera.DestinationType.DATA_URL,
                            sourceType: Camera.PictureSourceType.CAMERA,
                            quality: 75,
                            targetWidth: 640,
                            targetHeight: 480,
                            allowEdit: true,
                            encodingType: Camera.EncodingType.JPEG,
                            saveToPhotoAlbum: false,
                            popoverOptions: CameraPopoverOptions,
                            correctOrientation: true
                        };

                        $cordovaCamera.getPicture(options).then(function(data) {
                            $scope.business.business_photo_data = data;
                            $scope.capture1_image = "data:image/jpeg;base64," + data;
                            // $scope.update();
                        }, function (err) {
                            $ionicLoading.hide();
                        });
                    }else if(buttonIndex == 1){

                        var options = {
                            quality: 50,
                            destinationType: Camera.DestinationType.DATA_URL,
                            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                            allowEdit: false,
                            encodingType: Camera.EncodingType.JPEG,
                            popoverOptions: CameraPopoverOptions,
                            saveToPhotoAlbum: false
                        };
                        $ionicLoading.show({
                            template: 'Loading...'
                        });
                        $cordovaCamera.getPicture(options).then(function(data) {
                            $ionicLoading.hide();
                            $scope.business.business_photo_data = data;
                            $scope.capture1_image ="data:image/jpeg;base64," + data;
                            // $scope.update();
                        }, function (err) {
                            $ionicLoading.hide();
                        });
                    }
                    return true;
                },
                destructiveButtonClicked: function() {
                    return true;
                }
            });
        };


        
  
   })
   .controller('LabelCommentCtl', function($scope, $rootScope, $state, $cordovaSocialSharing, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService, User){
        $scope.init = function (){
            $scope.business=[];
           
            if($localStorage.active_label){
                $scope.active_label=JSON.parse($localStorage.active_label);
    
                console.log($scope.active_label);
                $scope.loadLabel_detail($scope.active_label);
            }
            
            
      
        };  
        $scope.loadLabel_detail = function (temp) {
              
            APIService.getLabel(
                
                temp,
                function(result){
                    if(result.status){
                        
                            $scope.labelComment=result.result;
                            console.log($scope.labelComment);
                            console.log(result.result);
                    }else{
                        $ionicPopup.alert({
                            title: 'Info',
                            template: result.msg//JSON.stringify(result.description.error)
                        });
                    }

                    
                },
                function (err) {
                    
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'Server connect error'//JSON.stringify(err)
                    });

                    
            });
        };
        $scope.goBack = function () {
            $state.go('comment-view');
        };
        $scope.goSingleMap = function () {
        //    $rootScope.$apply();
            $state.go('single-map');
        };
      
        $scope.isLoggedin=User.isLoggedIn();
        $scope.loadBusiness= function(callback){
    
         
            if($localStorage.active_business){
                var active_business=JSON.parse($localStorage.active_business);
                $scope.active_business=active_business;
                console.log(active_business);
            }
            
        };
        $scope.doRefresh = function () {
            $scope.loadBusiness(function(){
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        $scope.$on('$ionicView.afterEnter', function() {
            $scope.loadBusiness();
        });
          $scope.shareFacebook = function(item){
            try{
                $ionicLoading.show({template:'Loading Facebook...'});
                $cordovaSocialSharing.shareViaFacebook(item.label_title, null, null, item.label_content)
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
        $scope.shareTwitter = function(item){
           try{
                $ionicLoading.show({template:'Loading Twitter...'});
                $cordovaSocialSharing
                .shareViaTwitter(item.label_title, null, null, item.label_content)
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
        $scope.shareLinkedin = function(item){
            try{
                $ionicLoading.show({template:'Loading Linkedin...'});
                $cordovaSocialSharing
                .shareVia('linkedin',item.label_title, null, null, item.label_content)
                    .then(function(result) {
                        $ionicLoading.hide();
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title:'Error',
                            template:'Cannot share'
                        });
                    });
            }catch (err){
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Alert',
                    template: err.message
                });
            }
        }
  
  
   })
  .controller('MapCtrl', function($scope, $rootScope, $state, $cordovaSocialSharing, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService, User) {
        $scope.mapT = {entity: null};
        $scope.$on('$ionicView.afterEnter', function() {
            console.log($localStorage.advance_label);
          
        });
        
        $scope.forMap=JSON.parse($localStorage.forMap);

        var forCenterLng, forCenterLat;
        var centerLng = 0;
        var centerLat = 0;
        var glat, glong;
        var tempIndex;
   

        
        $scope.mapCreated = function(map) {
            console.log("locationFlag = " + $rootScope.locationFlag);
            
            if($rootScope.locaitonFlag){
                forCenterLat = globalMarkerLocation.lat();
                forCenterLng = globalMarkerLocation.lng();
                console.log(globalMarkerLocation.lng());
                
                
            }else{
                forCenterLat = searchCenterLat;
                forCenterLng = searchCenterLng;
            }
            centerLat = 0;
            centerLng = 0;
            tempIndex = 0;
            
       
          
            console.log("current correct Position="+ $rootScope.currentPosition.lng()+":"+$rootScope.currentPosition.lat());
            $scope.mapT.entity = map;  // this sets the map we just created in our directive to the $scope
            
            //   $scope.placeMarkers($scope.active_business.business_lati,$scope.active_business.business_long); // we *can* initialize the map with markers if we need to here
            console.log($scope.forMap);
            console.log($scope.forMap[0].business_lati, $scope.forMap[0].business_long);
            console.log($localStorage.advance_label);
            for(var i=0; i<$scope.forMap.length; i++){
                $scope.placeMarkers($scope.forMap[i]);
                
                // Getting for center of map
                console.log(forCenterLat);
                glat = $scope.forMap[i].business_lati;
                glong = $scope.forMap[i].business_long;
                console.log("glong=" + glong);
                 currentDistance = getDistanceFromLatLonInKm(forCenterLat, forCenterLng, glat, glong);
                
                console.log("currentDistance= "+currentDistance);
                if(currentDistance < 50){
                    centerLat = +centerLat + +glat;
                    centerLng = +centerLng + +glong;
                    console.log("glat=" + glat);
                    tempIndex ++;
                    console.log("tempIndex= " + tempIndex);
                }
               
                
            }
            console.log("TotalLat= "+ centerLat);
            console.log("TotalLng= "+ centerLng);
            console.log("tempIndex= " + tempIndex);
            centerLat /=tempIndex;
            centerLng /=tempIndex;
            console.log("Center of Map" + centerLat + ":" + centerLng);
            $scope.setMapCenter(centerLat, centerLng);
       };

       $scope.setMapCenter = function(glat, glong) {
            $scope.mapT.entity.setZoom(12);
            $scope.mapT.entity.setCenter(new google.maps.LatLng(glat, glong));
       };
        
    //    $scope.placeMarkers = function(business){ // this function will be responsible for setting the markers on the map
    //         var glat = business.business_lati;
    //         var glong = business.business_long;
    //         var markerPosition = new google.maps.LatLng(glat, glong); // this is the google api code that takes a latitude and longitude position
    //         var gmap_photo = business.business_photo_url;
    //         console.log(business);
          

    //         var canvas = document.createElement('canvas');
    //         var context = canvas.getContext("2d");
    //         var imageObj = new Image();
    //         var label = business.business_rate_score + ',' +business.business_rate_num;
            
    //         var favor_link = 'img/unfavorit.png';
    //         imageObj.src = "/img/unfavorit_map.png"; 
    //         if(typeof(business.favor_status) !== 'undefined' && business.favor_status > 0){
    //             favor_link = 'img/favorit.png';
    //             imageObj.src = "/img/favorit_map.png"; 
    //         }
  
   
    //         imageObj.onload = function(){
    //             context.drawImage(imageObj, 16, 0);

    //             //Adjustable parameters
    //             context.shadowColor = "#ffffff"
    //             context.font = "20px Arial";
    //             context.fillStyle = "white";
    //             context.fillText(label, 24, 31);
    //             //End

    //             var image = {
    //                 url: canvas.toDataURL(),
    //                 size: new google.maps.Size(80, 104),
    //                 origin: new google.maps.Point(0,0),
    //                 anchor: new google.maps.Point(0, 0)
    //             };
    //             // the clickable region of the icon.
        
    //             var marker = new google.maps.Marker({
    //                 position: markerPosition,
    //                 map: $scope.mapT.entity,
    //                 labelAnchor: new google.maps.Point(0, 0),
    //                 icon: image
        
    //             });
    //             google.maps.event.addListener(marker, 'click', function(){  // this listens for click events on the markers
                
    //                 if($scope.openInfoWindow){
    //                     $scope.openInfoWindow.close();
    //                 }
    //                 $scope.openInfoWindow = infoWindow;
    //                 $scope.openInfoWindow.open($scope.mapT.entity, marker);
    //             });
      
    //         };
          
    //         var html = '';
            
            
    //         var search_name = business.business_name;
    //         var search_type = '';
    //         var search_label = '';
    //         if(typeof(business.business_type) !== 'undefined'){ 
    //             search_type = '[' + business.business_type + ']';
    //         }
    //         for(var i= 0; i<business.label_detail.length; i++ ){
    //             if(business.label_detail[i].avg_rate==6){
    //                 search_label = search_label + '<li class="label-name_green">[...]</li>'; 
    //             }
    //             if((business.label_detail[i].avg_rate<6)&&(business.label_detail[i].avg_rate>3)){
    //                 search_label = search_label + '<li class="label-name_green">[' + business.label_detail[i].label_name + ']</li>'; 
    //             }
    //             if((business.label_detail[i].avg_rate<4)&&(business.label_detail[i].avg_rate>2)){
    //                 search_label = search_label + '<li class="label-name_orange">[' + business.label_detail[i].label_name + ']</li>';
    //             }
    //             if((business.label_detail[i].avg_rate>-1)&&(business.label_detail[i].avg_rate<3)){
    //                 search_label = search_label + '<li class="label-name_red">[' + business.label_detail[i].label_name + ']</li>'; 
    //             }
    //         }
        
            
    //         var infoWindow = new google.maps.InfoWindow({
    //             // we can also have a popup window active when clicked on
    //             photo_url : business.business_photo_url,
    //             content:    '<div class = "row">' +
    //                             '<div class = "col" >' +
    //                                 '<img src="'+ gmap_photo +'" class = "gmap-photo" />' +
                                    
    //                                 '<div class="circle-mark">' +
                                        
    //                                     '<div class="project-number">' + business.business_rate_score + '</div>' +
    //                                     '<div class="rate-number">,' + business.business_rate_num + '</div>' +
    //                                     '<div class="total">/5</div>' +
    //                                 '</div>' +
                                    
    //                             '</div>' +
    //                             '<div class="col detail">' +
    //                                 '<li class="bussiness-name">' + search_name +'</li>' +
    //                                 '<li class="bussiness-type">' + search_type +'</li>' +
    //                                 '<ul class="bussiness-label">' + search_label +'</ul>' +
    //                                 '<ul style="margin-top:40px;">' + 
    //                                     '<li><img src="' + favor_link + '" class="bussiness-favor"/></li>' + 
                                        
    //                                     '<li style="float:right; color: #89ac27; font-weight: 600; font-size: 14px; ">' + business.business_city + '</li>' +
    //                                     '<li style="float:right;"><img src="img/map_location.png" class="map-location"/></li>' + 
    //                                 '</ul>' +
    //                             '</div>' +
    //                         '</div>' 

    //         });
    //         console.log(infoWindow);


    //     }
         $scope.placeMarkers = function(business){ // this function will be responsible for setting the markers on the map
            var glat = business.business_lati;
            var glong = business.business_long;
            var markerPosition = new google.maps.LatLng(glat, glong); // this is the google api code that takes a latitude and longitude position
            var gmap_photo = business.business_photo_url;
            console.log(business);
          

            // var canvas = document.createElement('canvas');
            // var context = canvas.getContext("2d");
            // var imageObj = new Image();
            // var label = business.business_rate_score + ',' +business.business_rate_num;
            var marker_link = 'img/' + 'u' + business.business_rate_score*10 + '.png';
            var favor_link = 'img/unfavorit.png';
            // imageObj.src = "/img/unfavorit_map.png"; 
            if(typeof(business.favor_status) !== 'undefined' && business.favor_status > 0){
                favor_link = 'img/favorit.png';
                marker_link = 'img/' + 'f' + business.business_rate_score*10 + '.png';
                // imageObj.src = "/img/favorit_map.png"; 
            }
  
   
            // imageObj.onload = function(){
            //     context.drawImage(imageObj, 16, 0);

            //     //Adjustable parameters
            //     context.shadowColor = "#ffffff"
            //     context.font = "20px Arial";
            //     context.fillStyle = "white";
            //     context.fillText(label, 24, 31);
            //     //End

            //     var image = {
            //         url: canvas.toDataURL(),
            //         size: new google.maps.Size(80, 104),
            //         origin: new google.maps.Point(0,0),
            //         anchor: new google.maps.Point(0, 0)
            //     };
            //     // the clickable region of the icon.
        
               
      
            // };
          
            var html = '';
             var marker = new google.maps.Marker({
                    position: markerPosition,
                    map: $scope.mapT.entity,
                    labelAnchor: new google.maps.Point(0, 0),
                    icon: marker_link
        
                });
                google.maps.event.addListener(marker, 'click', function(){  // this listens for click events on the markers
                
                    if($scope.openInfoWindow){
                        $scope.openInfoWindow.close();
                    }
                    $scope.openInfoWindow = infoWindow;
                    $scope.openInfoWindow.open($scope.mapT.entity, marker);
                });
            
            var search_name = business.business_name;
            var search_type = '';
            var search_label = '';
            if(typeof(business.business_type) !== 'undefined'){ 
                search_type = '[' + business.business_type + ']';
            }
            for(var i= 0; i<business.label_detail.length; i++ ){
                if(business.label_detail[i].avg_rate==6){
                    search_label = search_label + '<li class="label-name_green">[...]</li>'; 
                }
                if((business.label_detail[i].avg_rate<6)&&(business.label_detail[i].avg_rate>3)){
                    search_label = search_label + '<li class="label-name_green">[' + business.label_detail[i].label_name + ']</li>'; 
                }
                if((business.label_detail[i].avg_rate<4)&&(business.label_detail[i].avg_rate>2)){
                    search_label = search_label + '<li class="label-name_orange">[' + business.label_detail[i].label_name + ']</li>';
                }
                if((business.label_detail[i].avg_rate>-1)&&(business.label_detail[i].avg_rate<3)){
                    search_label = search_label + '<li class="label-name_red">[' + business.label_detail[i].label_name + ']</li>'; 
                }
            }
        
            
            var infoWindow = new google.maps.InfoWindow({
                // we can also have a popup window active when clicked on
                photo_url : business.business_photo_url,
                content:    '<div class = "row">' +
                                '<div class = "col" >' +
                                    '<img src="'+ gmap_photo +'" class = "gmap-photo" />' +
                                    
                                    '<div class="circle-mark">' +
                                        
                                        '<div class="project-number">' + business.business_rate_score + '</div>' +
                                        // '<div class="rate-number">,' + business.business_rate_num + '</div>' +
                                        '<div class="total">/5</div>' +
                                    '</div>' +
                                    
                                '</div>' +
                                '<div class="col detail">' +
                                    '<li class="bussiness-name">' + search_name +'</li>' +
                                    '<li class="bussiness-type">' + search_type +'</li>' +
                                    '<ul class="bussiness-label">' + search_label +'</ul>' +
                                    '<ul style="margin-top:40px;">' + 
                                        '<li><img src="' + favor_link + '" class="bussiness-favor"/></li>' + 
                                        
                                        '<li class="bussiness-city">' + business.business_city + '</li>' +
                                        '<li style="float:right;"><img src="img/map_location.png" class="map-location"/></li>' + 
                                    '</ul>' +
                                '</div>' +
                            '</div>' 

            });
            console.log(infoWindow);


        }
   
        $scope.goBack = function () {
            $state.go('search-result');
        };
        $scope.goSearchMap = function () {
            $state.go('search-map');
        };
   })
   .controller('SingleMapCtrl', function($scope, $rootScope, $state, $cordovaSocialSharing, $localStorage, $ionicHistory, $ionicLoading, $ionicPopup, APIService, User) {
        $scope.mapT = {entity: null};

        $scope.mapCreated = function(map) {
          if($localStorage.active_business){
               var active_business=JSON.parse($localStorage.active_business);
               $scope.active_business=active_business;
          }
          
          console.log($scope.active_business);
          $scope.mapT.entity = map;  // this sets the map we just created in our directive to the $scope
          $scope.setMapCenter($scope.active_business.business_lati, $scope.active_business.business_long);
          $scope.placeMarkers($scope.active_business); // we *can* initialize the map with markers if we need to here
        };

       $scope.setMapCenter = function(glat, glong) {
           $scope.mapT.entity.setZoom(14);
            $scope.mapT.entity.setCenter(new google.maps.LatLng(glat, glong));
       };
        
       $scope.placeMarkers = function(business){ // this function will be responsible for setting the markers on the map
            var glat = business.business_lati;
            var glong = business.business_long;
            var markerPosition = new google.maps.LatLng(glat, glong); // this is the google api code that takes a latitude and longitude position
            var gmap_photo = business.business_photo_url;
            console.log(business);
          

            // var canvas = document.createElement('canvas');
            // var context = canvas.getContext("2d");
            // var imageObj = new Image();
            // var label = business.business_rate_score + ',' +business.business_rate_num;
            var marker_link = 'img/' + 'u' + business.business_rate_score*10 + '.png';
            var favor_link = 'img/unfavorit.png';
            // imageObj.src = "/img/unfavorit_map.png"; 
            if(typeof(business.favor_status) !== 'undefined' && business.favor_status > 0){
                favor_link = 'img/favorit.png';
                marker_link = 'img/' + 'f' + business.business_rate_score*10 + '.png';
                // imageObj.src = "/img/favorit_map.png"; 
            }
  
   
            // imageObj.onload = function(){
            //     context.drawImage(imageObj, 16, 0);

            //     //Adjustable parameters
            //     context.shadowColor = "#ffffff"
            //     context.font = "20px Arial";
            //     context.fillStyle = "white";
            //     context.fillText(label, 24, 31);
            //     //End

            //     var image = {
            //         url: canvas.toDataURL(),
            //         size: new google.maps.Size(80, 104),
            //         origin: new google.maps.Point(0,0),
            //         anchor: new google.maps.Point(0, 0)
            //     };
            //     // the clickable region of the icon.
        
               
      
            // };
          
            var html = '';
             var marker = new google.maps.Marker({
                    position: markerPosition,
                    map: $scope.mapT.entity,
                    labelAnchor: new google.maps.Point(0, 0),
                    icon: marker_link
        
                });
                google.maps.event.addListener(marker, 'click', function(){  // this listens for click events on the markers
                
                    if($scope.openInfoWindow){
                        $scope.openInfoWindow.close();
                    }
                    $scope.openInfoWindow = infoWindow;
                    $scope.openInfoWindow.open($scope.mapT.entity, marker);
                });
            
            var search_name = business.business_name;
            var search_type = '';
            var search_label = '';
            if(typeof(business.business_type) !== 'undefined'){ 
                search_type = '[' + business.business_type + ']';
            }
            for(var i= 0; i<business.label_detail.length; i++ ){
                if(business.label_detail[i].avg_rate==6){
                    search_label = search_label + '<li class="label-name_green">[...]</li>'; 
                }
                if((business.label_detail[i].avg_rate<6)&&(business.label_detail[i].avg_rate>3)){
                    search_label = search_label + '<li class="label-name_green">[' + business.label_detail[i].label_name + ']</li>'; 
                }
                if((business.label_detail[i].avg_rate<4)&&(business.label_detail[i].avg_rate>2)){
                    search_label = search_label + '<li class="label-name_orange">[' + business.label_detail[i].label_name + ']</li>';
                }
                if((business.label_detail[i].avg_rate>-1)&&(business.label_detail[i].avg_rate<3)){
                    search_label = search_label + '<li class="label-name_red">[' + business.label_detail[i].label_name + ']</li>'; 
                }
            }
        
            
            var infoWindow = new google.maps.InfoWindow({
                // we can also have a popup window active when clicked on
                photo_url : business.business_photo_url,
                content:    '<div class = "row">' +
                                '<div class = "col" >' +
                                    '<img src="'+ gmap_photo +'" class = "gmap-photo" />' +
                                    
                                    '<div class="circle-mark">' +
                                        
                                        '<div class="project-number">' + business.business_rate_score + '</div>' +
                                        // '<div class="rate-number">,' + business.business_rate_num + '</div>' +
                                        '<div class="total">/5</div>' +
                                    '</div>' +
                                    
                                '</div>' +
                                '<div class="col detail">' +
                                    '<li class="bussiness-name">' + search_name +'</li>' +
                                    '<li class="bussiness-type">' + search_type +'</li>' +
                                    '<ul class="bussiness-label">' + search_label +'</ul>' +
                                    '<ul style="margin-top:40px;">' + 
                                        '<li><img src="' + favor_link + '" class="bussiness-favor"/></li>' + 
                                        
                                        '<li class="bussiness-city">' + business.business_city + '</li>' +
                                        '<li style="float:right;"><img src="img/map_location.png" class="map-location"/></li>' + 
                                    '</ul>' +
                                '</div>' +
                            '</div>' 

            });
            console.log(infoWindow);


        }
 
   
        $scope.goBack = function () {
            $state.go('comment-view');
        };
        $scope.goSearchMap = function () {
            $state.go('leave-comment');
        };
   })
   
   



    .controller('OfflineCtrl', function ($scope, $location, $cordovaNetwork, $ionicLoading, $ionicSideMenuDelegate, $ionicGesture, $ionicHistory) {})
   ;
